public class FormaGeometricaFactory {
    public static FormaGeometrica criar(int tipo, int x, int y) {
        FormaGeometrica formaGeometrica = null;
        switch (tipo) {
            // if (tipo == 1) { }
            case 1:
                // criar um retangulo
                formaGeometrica = new RetanguloImpl();
                break;
            case 2:
                // criar um quadrado
                formaGeometrica = new QuadradoImpl();
                break;
        }
        formaGeometrica.setX(x);
        formaGeometrica.setY(y);
        return formaGeometrica;
    }
}