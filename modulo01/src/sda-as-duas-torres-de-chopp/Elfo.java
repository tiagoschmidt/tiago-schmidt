// C#: public sealed class Elfo
// java: public final class
// C# public class Elfo : Personagem
public class Elfo extends Personagem {
    protected int experiencia;
    protected int QTD_EXPERIENCIA;
    private static int qtdElfos;
    // type initializer
    {
        experiencia = 0;
        QTD_EXPERIENCIA = 1;
        QTD_DANO = 0.;
        super.ganharItem(new Item("Arco", 1));
        super.ganharItem(new Item("Flecha", 7));
        Elfo.qtdElfos++;
    }

    public Elfo(String nomeInformado) {
        super(nomeInformado, 100.0, Status.VIVO);
    }
    
    protected void finalize() throws Throwable {
        Elfo.qtdElfos--;
    }

    public void atirarFlecha(Dwarf dwarf) {
        Item flecha = getFlecha();
        if (flecha.getQuantidade() > 0) {
            flecha.setQuantidade(flecha.getQuantidade() - 1);
            experiencia += this.QTD_EXPERIENCIA;
            dwarf.perderVida();
            this.perderVida();
        }
    }
    
    public static int getQtdElfos() {
        return Elfo.qtdElfos;
    }

    public Item getFlecha() {
        return this.inventario.buscar("Flecha");
    }
    // Law of Demeter
    // elfo.getFlecha().getQuantidade();
    // elfo.getQtdFlechas();

    public int getExperiencia() {
        return this.experiencia;
    }
    
    public void dizerOi() {
        System.out.println("Elfo diz oi!");
    }
    
    public int hashCode() {
        return this.nome.hashCode();
    }
}