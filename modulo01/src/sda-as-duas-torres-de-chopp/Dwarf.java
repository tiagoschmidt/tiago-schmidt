public class Dwarf extends Personagem {

    public Dwarf(String nome) {
        super(nome, 110.0, Status.VIVO);
        QTD_DANO = 10.0;
    }

    public void dizerOi() {
        System.out.println("Oi, eu sou um Dwarf");
    }
}