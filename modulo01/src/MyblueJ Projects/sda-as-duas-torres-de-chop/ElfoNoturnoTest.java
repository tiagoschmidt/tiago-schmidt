import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Arrays;

public class ElfoNoturnoTest {
    private final double DELTA = 0.1;
    
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void testaSeJaNasceuComArcoFlechaNoInventario(){
        Personagem elfoDaNoite = new ElfoNoturno("Dark");
        
        ArrayList <Item> esperado = new ArrayList(Arrays.asList(new Item("Arco",1),new Item("Flecha",12)));
        
        assertEquals(esperado,elfoDaNoite.inventario.getItens());
    }
    
    @Test
    public void elfoNoturnoAtirandoQuatroFlechasExpDeveSer12(){
        Elfo elfoDaNoite = new ElfoNoturno("Lich");
        Personagem dwarf = new Dwarf("Bombur");
        
        for(int i=0; i<4; i++){
            elfoDaNoite.atirarFlecha((Dwarf)dwarf);
        }
    
        assertEquals(12,elfoDaNoite.getExperiencia());
    }
    
    @Test
    public void elfoNoturnoAtirandoQuatroFlechasVidaDeveSer40(){
        Elfo elfoDaNoite = new ElfoNoturno("Lich");
        Personagem dwarf = new Dwarf("Bombur");
        
        for(int i=0; i<4; i++){
            elfoDaNoite.atirarFlecha((Dwarf)dwarf);
        }
    
        assertEquals(40,elfoDaNoite.getVida(),DELTA);
    }
    
    @Test
    public void elfoNoturnoAtiraSeteFlechasDeveEstarMorto(){
        Elfo elfoDaNoite = new ElfoNoturno("Lich");
        Personagem dwarf = new Dwarf("Bombur");
        
        for(int i=0; i<7; i++){
            elfoDaNoite.atirarFlecha((Dwarf)dwarf);
        }
    
        assertEquals(Status.MORTO,elfoDaNoite.getStatus());
    }
    
    @Test
    public void elfoNoturnoTentaAtirarUmaFlechaDepoisDeMorto(){
        Elfo elfoDaNoite = new ElfoNoturno("Lich");
        Personagem dwarf = new Dwarf("Bombur");
        Item flecha = elfoDaNoite.inventario.buscar("Flecha");
        
        for(int i=0; i<8; i++){
            elfoDaNoite.atirarFlecha((Dwarf)dwarf);
        }
    
        assertEquals(Status.MORTO,elfoDaNoite.getStatus());
        assertEquals(5,flecha.getQuantidade());
    }
}
