import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;

public class PaginadorInventarioTest{
    @Test
    public void testaPaginadorComPular0Limitar2DeveRetornarEspadaEscudo(){
        Inventario inventario = new Inventario();
        ArrayList <Item> listaPaginada = new ArrayList <>();
        
        inventario.adicionar(new Item("Espada", 1));
        inventario.adicionar(new Item("Escudo de metal", 2));
        inventario.adicionar(new Item("Poção de HP", 3));
        inventario.adicionar(new Item("Bracelete", 1));
        
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        
        paginador.pular(0);
        listaPaginada = paginador.limitar(2); 
        
        assertEquals("Espada",listaPaginada.get(0).getDescricao());
        assertEquals("Escudo de metal",listaPaginada.get(1).getDescricao());
    }
    
    @Test
    public void testaPaginadorComPular3Limitar3DeveRetornarSomenteBracelete(){
        Inventario inventario = new Inventario();
        ArrayList <Item> listaPaginada = new ArrayList <>();
        
        inventario.adicionar(new Item("Espada", 1));
        inventario.adicionar(new Item("Escudo de metal", 2));
        inventario.adicionar(new Item("Poção de HP", 3));
        inventario.adicionar(new Item("Bracelete", 1));
        
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        
        paginador.pular(3);
        listaPaginada = paginador.limitar(3);
        
        assertEquals("Bracelete",listaPaginada.get(0).getDescricao());
    }
    
    @Test
    public void testaPaginadorComPularMaiorOuIgualQtdeDeItensDeveRetornarNull(){
        Inventario inventario = new Inventario();
        ArrayList <Item> listaPaginada = new ArrayList <>();
        
        inventario.adicionar(new Item("Espada", 1));
        inventario.adicionar(new Item("Escudo de metal", 2));
        inventario.adicionar(new Item("Poção de HP", 3));
        inventario.adicionar(new Item("Bracelete", 1));
        
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        
        paginador.pular(4);
        listaPaginada = paginador.limitar(1);
        
        assertEquals(null,listaPaginada);
    }
    
    @Test
    public void testaPaginadorComListaVaziaDeveRetornarNull(){
        Inventario inventario = new Inventario();
        ArrayList <Item> listaPaginada = new ArrayList <>();
        
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        
        paginador.pular(2);
        listaPaginada = paginador.limitar(2);
        
        assertEquals(null,listaPaginada);
    }
}
