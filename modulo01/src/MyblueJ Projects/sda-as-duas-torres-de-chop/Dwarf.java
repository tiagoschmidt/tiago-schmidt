public class Dwarf extends Personagem {
    
    {
        this.dano = 10;
    }
    
    protected Dwarf(String nomeInformado){
        super(nomeInformado, 110.0, Status.VIVO);
    }
}