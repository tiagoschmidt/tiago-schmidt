import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EstatisticasInventarioTest {
    private final double DELTA = 0.001;
    
    @Test
    public void calculaMediaResultadoDeveSer5Ponto75(){        
        Inventario mochilaTeste = new Inventario();
        
        mochilaTeste.adicionar(new Item("Cavalos",3));
        mochilaTeste.adicionar(new Item("Flechas",8));
        mochilaTeste.adicionar(new Item("Arco",2));
        mochilaTeste.adicionar(new Item("Ouro",10));
    
        EstatisticasInventario estatisticas = new EstatisticasInventario(mochilaTeste);
    
        double resultadoMedia = estatisticas.calcularMedia();
    
        assertEquals(5.75,resultadoMedia,DELTA);
    }
        
    @Test
    public void calculaMediaListaVaziaDeveSer0(){        
        Inventario mochilaTeste = new Inventario();
    
        EstatisticasInventario estatisticas = new EstatisticasInventario(mochilaTeste);
    
        double resultadoMedia = estatisticas.calcularMedia();
    
        assertEquals(0,resultadoMedia,DELTA);
    }
    
    @Test
    public void calculaMediaTendoApenasUmItemDeveSerQtdeDesteItem(){        
        Inventario mochilaTeste = new Inventario();
        
        mochilaTeste.adicionar(new Item("Cavalos",3));
    
        EstatisticasInventario estatisticas = new EstatisticasInventario(mochilaTeste);
    
        double resultadoMedia = estatisticas.calcularMedia();
    
        assertEquals(3,resultadoMedia,DELTA);
    }
    
    @Test
    public void calculaMedianaTendoApenasUmItemDeveSerQtdeDesteItem(){        
        Inventario mochilaTeste = new Inventario();
        
        mochilaTeste.adicionar(new Item("Cavalos",3));
    
        EstatisticasInventario estatisticas = new EstatisticasInventario(mochilaTeste);
    
        double resultadoMediana = estatisticas.calcularMediana();
    
        assertEquals(3,resultadoMediana,DELTA);
    }
    
    @Test
    public void calculaMedianaComListaVaziaDeveSer0(){        
        Inventario mochilaTeste = new Inventario();
    
        EstatisticasInventario estatisticas = new EstatisticasInventario(mochilaTeste);
    
        double resultadoMediana = estatisticas.calcularMediana();
    
        assertEquals(0,resultadoMediana,DELTA);
    }
    
    @Test
    public void calculaMedianaComQtdeParDeItensResultadoDeveSer3Ponto5(){        
        Inventario mochilaTeste = new Inventario();
        
        mochilaTeste.adicionar(new Item("Cavalos",4));
        mochilaTeste.adicionar(new Item("Flechas",4));
        mochilaTeste.adicionar(new Item("Arco",1));
        mochilaTeste.adicionar(new Item("Espada",5));
        mochilaTeste.adicionar(new Item("Escudo",3));
        mochilaTeste.adicionar(new Item("Poção",1));
    
        EstatisticasInventario estatisticas = new EstatisticasInventario(mochilaTeste);
    
        double resultadoMediana = estatisticas.calcularMediana();
    
        assertEquals(3.5,resultadoMediana,DELTA);
    }
    
    @Test
    public void calculaMedianaComQtdeImparDeItensResultadoDeveSer3(){        
        Inventario mochilaTeste = new Inventario();
        
        mochilaTeste.adicionar(new Item("Cavalos",4));
        mochilaTeste.adicionar(new Item("Flechas",4));
        mochilaTeste.adicionar(new Item("Arco",1));
        mochilaTeste.adicionar(new Item("Escudo",3));
        mochilaTeste.adicionar(new Item("Poção",1));
    
        EstatisticasInventario estatisticas = new EstatisticasInventario(mochilaTeste);
    
        double resultadoMediana = estatisticas.calcularMediana();
    
        assertEquals(3,resultadoMediana,DELTA);
    }
    
    @Test
    public void verificaItensAcimaDaMediaTendoApenasUmItemDeveSer0(){        
        Inventario mochilaTeste = new Inventario();
        
        mochilaTeste.adicionar(new Item("Cavalos",3));
    
        EstatisticasInventario estatisticas = new EstatisticasInventario(mochilaTeste);
    
        int acimaDaMedia = estatisticas.qtdItensAcimaDaMedia();
    
        assertEquals(0,acimaDaMedia);
    }
    
    @Test
    public void verificaItensAcimaDaMediaDeveSer3(){        
        Inventario mochilaTeste = new Inventario();
        
        mochilaTeste.adicionar(new Item("Cavalos",4));
        mochilaTeste.adicionar(new Item("Flechas",4));
        mochilaTeste.adicionar(new Item("Arco",1));
        mochilaTeste.adicionar(new Item("Espada",5));
        mochilaTeste.adicionar(new Item("Escudo",3));
        mochilaTeste.adicionar(new Item("Poção",1));
    
        EstatisticasInventario estatisticas = new EstatisticasInventario(mochilaTeste);
    
        int acimaDaMedia = estatisticas.qtdItensAcimaDaMedia();
    
        assertEquals(3,acimaDaMedia);
    }
    
    @Test
    public void verificaItensAcimaDaMediaComListaVaziaDeveSer0(){        
        Inventario mochilaTeste = new Inventario();
    
        EstatisticasInventario estatisticas = new EstatisticasInventario(mochilaTeste);
    
        int acimaDaMedia = estatisticas.qtdItensAcimaDaMedia();
    
        assertEquals(0,acimaDaMedia);
    }
}
