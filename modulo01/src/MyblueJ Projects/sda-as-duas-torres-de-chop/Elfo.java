public class Elfo extends Personagem {
    protected int experiencia;
    protected int ganhoExp;
    public static int qtdeElfosInstanciados;
    
    // type initializer
    {
        this.ganhoExp = 1;
        this.experiencia = 0;
        this.inventario.adicionar(new Item("Arco",1));
        this.inventario.adicionar(new Item("Flecha",12));
    }
    
    //contrutores
    protected Elfo(String nomeInformado) {
        super(nomeInformado, 100.0, Status.VIVO);
        qtdeElfosInstanciados++;
    }
    
    //getters
    public int getExperiencia() {
        return this.experiencia;
    }
    
    public static int getQtdeElfosInstanciados() {
        return qtdeElfosInstanciados;
    }
    
    //atira uma flecha
    public void atirarFlecha(Dwarf inimigo) {
        Item flecha = this.inventario.buscar("Flecha");
        
        if (flecha.getQuantidade() > 0 && inimigo.getStatus()==Status.VIVO && this.status == Status.VIVO) {
            flecha.setQuantidade(flecha.getQuantidade() - 1);
            inimigo.perderVida();
            this.perderVida();
            this.experiencia += ganhoExp;
        }        
    }
    
    public void finalize() throws Throwable{
        qtdeElfosInstanciados--;
    }
}