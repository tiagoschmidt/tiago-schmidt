import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Arrays;

public class ElfoVerdeTest {
    
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void testaSeJaNasceuComArcoFlechaNoInventario(){
        Personagem elfoVerde = new ElfoVerde("Mr. Green");
        
        ArrayList <Item> esperado = new ArrayList(Arrays.asList(new Item("Arco",1),new Item("Flecha",12)));
        
        assertEquals(esperado,elfoVerde.inventario.getItens());
    }
    
    @Test
    public void testaGanhoExpAtirandoTresFlechasExpDeveSer6(){
        Elfo elfoVerde = new ElfoVerde("Mr. Green");
        Personagem dwarf = new Dwarf("Bombur");
        
        elfoVerde.atirarFlecha((Dwarf)dwarf);
        elfoVerde.atirarFlecha((Dwarf)dwarf);
        elfoVerde.atirarFlecha((Dwarf)dwarf);
    
        assertEquals(6,elfoVerde.getExperiencia());
    }
    
    @Test
    public void elfoVerdeTentaGanharUmaEspadaAndEscudoInventarioDeveSerApenasArcoFlecha(){
        Elfo verdun = new ElfoVerde("Piccolo");
        
        Item espada = new Item("Espada de Vidro",2);
        Item escudo = new Item("Escudo de Aço Valiriano",1);
    
        verdun.ganharItem(espada);
        verdun.ganharItem(escudo);
        
        ArrayList <Item> esperado = new ArrayList(Arrays.asList(new Item("Arco",1),new Item("Flecha",12)));
        
        assertEquals(esperado,verdun.inventario.getItens());
    }
    
    @Test
    public void elfoVerdeTentaPerderArcoFlechaDeveContinuarTendoArcoFlechaNoInventario(){
        Elfo verdun = new ElfoVerde("Piccolo");
    
        Item arco = new Item("Arco",1);
        Item flecha = new Item("Flecha",12);
        
        verdun.perderItem(arco);
        verdun.perderItem(flecha);
        
        ArrayList <Item> esperado = new ArrayList(Arrays.asList(arco,flecha));
        
        assertEquals(esperado,verdun.inventario.getItens());
    }
    
    @Test
    public void elfoVerdeGanhaArcoAndFlechaDeVidroDeveTerArcoFlechaArcoDeVidroFlechaVidroNoInventario(){
        Elfo verdun = new ElfoVerde("Piccolo");
    
        Item arco = new Item("Arco de Vidro",1);
        Item flecha = new Item("Flecha de Vidro",8);
        
        verdun.ganharItem(arco);
        verdun.ganharItem(flecha);
        
        ArrayList <Item> esperado = new ArrayList(Arrays.asList((new Item("Arco",1)),(new Item("Flecha",12)),arco,flecha));
        
        assertEquals(esperado,verdun.inventario.getItens());
    }
}
