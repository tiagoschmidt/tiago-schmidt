import java.util.ArrayList;

public class PaginadorInventario {
    Inventario paginador;
    int marcador = 0;
    
    public PaginadorInventario(Inventario inventario){
        this.paginador = inventario;
    }
    
    public void pular(int marcador){
        this.marcador = marcador;
    }
    
    public ArrayList<Item> limitar(int n){
        ArrayList <Item> paginacao = new ArrayList <>();
        int posicao = 0;
        
        if(!paginador.mochila.isEmpty() && marcador < paginador.mochila.size()){
            for(int i = marcador; i < i+n && i < paginador.mochila.size(); i++){
                    paginacao.add(posicao,paginador.mochila.get(i));
                    posicao++;
            }
        } else return null;       
        
        return paginacao;
    }
}
