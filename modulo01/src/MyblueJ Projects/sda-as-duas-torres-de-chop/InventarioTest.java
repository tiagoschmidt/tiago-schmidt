import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;

public class InventarioTest {    
    @Test
    public void posicoes012DaMochilaDevemTerArco1Flecha7Escudo1(){
        Item arco = new Item("Arco",1);
        Item flecha = new Item("Flecha",7);
        Item escudo = new Item("Escudo",1);
        
        Inventario mochilaTeste = new Inventario();
        
        mochilaTeste.adicionar(arco);
        mochilaTeste.adicionar(flecha);
        mochilaTeste.adicionar(escudo);
        
        assertEquals("Arco",mochilaTeste.mochila.get(0).getDescricao());
        assertEquals(1,mochilaTeste.mochila.get(0).getQuantidade());
        assertEquals("Flecha",mochilaTeste.mochila.get(1).getDescricao());
        assertEquals(7,mochilaTeste.mochila.get(1).getQuantidade());
        assertEquals("Escudo",mochilaTeste.mochila.get(2).getDescricao());
        assertEquals(1,mochilaTeste.mochila.get(2).getQuantidade());
    }
    
    @Test
    public void posicoes012DaMochilaNaoDevemTerArco2Flechas10Null(){
        Item arco = new Item("Arco",10);
        Item flecha = new Item("Flecha",5);
        Item vazio = new Item("",1);
        
        Inventario mochilaTeste = new Inventario();
        
        mochilaTeste.adicionar(arco);
        mochilaTeste.adicionar(flecha);
        mochilaTeste.adicionar(vazio);
        
        ArrayList <Item> listaTeste = mochilaTeste.getItens();
        
        assertNotEquals(2,listaTeste.get(0).getQuantidade());
        assertNotEquals("Flechas",listaTeste.get(1).getDescricao());
        assertNotEquals(10,listaTeste.get(1).getQuantidade());
        assertNotEquals(null,listaTeste.get(2).getDescricao());
    }
   
    @Test
    public void obtendoUmItemDeveSerCavalo2(){
        Item cavalo = new Item("Cavalo",2);
        
        Inventario mochilaTeste = new Inventario();
        
        mochilaTeste.adicionar(cavalo);
        
        Item itemEsperado = mochilaTeste.obter(0);
        
        assertEquals("Cavalo",itemEsperado.getDescricao());
        assertEquals(2,itemEsperado.getQuantidade());
    }
    
    @Test
    public void obtendoUmItemNaoDeveSerEspadas4(){
        Item espadas = new Item("Espada",5);
        
        Inventario mochilaTeste = new Inventario();
        
        mochilaTeste.adicionar(espadas);
        
        Item itemEsperado = mochilaTeste.obter(0);
        
        assertNotEquals("Espadas",itemEsperado.getDescricao());
        assertNotEquals(4,itemEsperado.getQuantidade());
    }
        
    @Test
    public void removerItemQtdeItensNaMochilaDeveSer1(){
        Item arco = new Item("Arco",10);
        Item flecha = new Item("Flecha",5);
        
        Inventario mochilaTeste = new Inventario();
        
        mochilaTeste.adicionar(arco);
        mochilaTeste.adicionar(flecha);
        mochilaTeste.remover(1);
        
        assertEquals(1,mochilaTeste.getItensNaMochila());
    }    
    
    @Test
    public void stringDescricoesDosItensDeveSerArcoFlechaEscudo(){
        Item arco = new Item("Arco",1);
        Item flecha = new Item("Flecha",7);
        Item escudo = new Item("Escudo",1);
        
        Inventario mochilaTeste = new Inventario();
        
        mochilaTeste.adicionar(arco);
        mochilaTeste.adicionar(flecha);
        mochilaTeste.adicionar(escudo);
        
        assertEquals("Arco,Flecha,Escudo",mochilaTeste.getDescricoesItens().toString());
    }
    
    @Test
    public void buscarItemDeveSerCavalo3(){
        Item cavalo = new Item("Cavalo",3);
        Item itemEsperado;
        
        Inventario mochilaTeste = new Inventario();
        
        mochilaTeste.adicionar(cavalo);
        
        itemEsperado = mochilaTeste.buscar("Cavalo");
        
        assertEquals("Cavalo",itemEsperado.getDescricao());
        assertEquals(3,itemEsperado.getQuantidade());    
    }
    
    @Test
    public void buscarItemQueNaoExisteDeveSerNull(){
        Item arco = new Item("Arco",1);
        Item flecha = new Item("Flecha",3);
        Item itemEsperado;
        
        Inventario mochilaTeste = new Inventario();
        
        mochilaTeste.adicionar(arco);
        mochilaTeste.adicionar(flecha);
        
        itemEsperado = mochilaTeste.buscar("Cavalo");
        
        assertEquals(null,itemEsperado);
    }
    
    @Test
    public void buscarItemComArrayListVazioDeveSerNull(){
        Item itemEsperado;
        
        Inventario mochilaTeste = new Inventario();
        
        itemEsperado = mochilaTeste.buscar("Item Qualquer");
        
        assertEquals(null,itemEsperado);
    }
    
    @Test
    public void buscarDoisItensComMesmoNomeDeveRetornarArcoCom1Qtde(){
        Item arco1 = new Item("Arco",1);
        Item arco2 = new Item("Arco",3);
        Item arco3 = new Item("Arco",5);
        
        Item itemEsperado;
        
        Inventario mochilaTeste = new Inventario();
        
        mochilaTeste.adicionar(arco1);
        mochilaTeste.adicionar(arco2);
        mochilaTeste.adicionar(arco3);
        
        itemEsperado = mochilaTeste.buscar("Arco");
        
        assertEquals("Arco",itemEsperado.getDescricao());
        assertEquals(1,itemEsperado.getQuantidade()); 
    }
    
    @Test
    public void inverterListaDeveSerEspadaEscudoFlechaArco(){
        Inventario mochilaTeste = new Inventario();
        
        mochilaTeste.adicionar(new Item("Arco",1));
        mochilaTeste.adicionar(new Item("Flecha",1));
        mochilaTeste.adicionar(new Item("Escudo",1));
        mochilaTeste.adicionar(new Item("Espada",1));
    
        ArrayList <Item> listaInvertida = mochilaTeste.inverter();
        
        assertEquals("Espada",listaInvertida.get(0).getDescricao());
        assertEquals("Escudo",listaInvertida.get(1).getDescricao());
        assertEquals("Flecha",listaInvertida.get(2).getDescricao());
        assertEquals("Arco",listaInvertida.get(3).getDescricao());
    }
    
    @Test
    public void inverterListaNaoDeveAlterarListaPrincipal(){
        Inventario mochilaTeste = new Inventario();
        
        mochilaTeste.adicionar(new Item("Arco",1));
        mochilaTeste.adicionar(new Item("Flecha",1));
        mochilaTeste.adicionar(new Item("Escudo",1));
    
        mochilaTeste.inverter();
        
        ArrayList listaTeste = mochilaTeste.getItens();
        
        assertArrayEquals(mochilaTeste.getItens().toArray(),listaTeste.toArray());  
    }
    
    @Test
    public void inverterListaComApenasUmElementoDeveRetornarListaIgual(){
        Inventario mochilaTeste = new Inventario();
        
        mochilaTeste.adicionar(new Item("Lança",2));
    
        ArrayList <Item> listaInvertida = mochilaTeste.inverter();
        
        assertEquals("Lança",listaInvertida.get(0).getDescricao());
    }
    
    @Test
    public void tentarInverterComListaVaziaDeveSerNull(){
        Inventario mochilaTeste = new Inventario();
    
        ArrayList <Item> listaInvertida = mochilaTeste.inverter();
        
        assertEquals(null,listaInvertida);
    }
    
    @Test
    public void ordenarItensFormaAscendenteTotalmenteDesordenadoDeveSerArcoEscEspFlecBracFacaOuroPocoes(){
        Inventario mochilaTeste = new Inventario();
        
        mochilaTeste.adicionar(new Item("Poções",6));
        mochilaTeste.adicionar(new Item("Flecha",3));
        mochilaTeste.adicionar(new Item("Ouro",5));
        mochilaTeste.adicionar(new Item("Espada",2));
        mochilaTeste.adicionar(new Item("Arco",1));
        mochilaTeste.adicionar(new Item("Escudo",1));
        mochilaTeste.adicionar(new Item("Braceletes",3));
        mochilaTeste.adicionar(new Item("Faca",4));
        

        mochilaTeste.ordenarItens();
    
        assertEquals("Arco",mochilaTeste.mochila.get(0).getDescricao());
        assertEquals("Escudo",mochilaTeste.mochila.get(1).getDescricao());
        assertEquals("Espada",mochilaTeste.mochila.get(2).getDescricao());
        assertEquals("Flecha",mochilaTeste.mochila.get(3).getDescricao());
        assertEquals("Braceletes",mochilaTeste.mochila.get(4).getDescricao());
        assertEquals("Faca",mochilaTeste.mochila.get(5).getDescricao());
        assertEquals("Ouro",mochilaTeste.mochila.get(6).getDescricao());
        assertEquals("Poções",mochilaTeste.mochila.get(7).getDescricao());
    }
    
    @Test
    public void ordenarItensFormaAscendenteArrayJaOrdenadoDeveSerArcoEscudoEspadaFlechaFaca(){
        Inventario mochilaTeste = new Inventario();
        
        mochilaTeste.adicionar(new Item("Arco",1));
        mochilaTeste.adicionar(new Item("Escudo",1));
        mochilaTeste.adicionar(new Item("Espada",2));
        mochilaTeste.adicionar(new Item("Flecha",3));
        mochilaTeste.adicionar(new Item("Faca",4));

        mochilaTeste.ordenarItens();
    
        assertEquals("Arco",mochilaTeste.mochila.get(0).getDescricao());
        assertEquals("Escudo",mochilaTeste.mochila.get(1).getDescricao());
        assertEquals("Espada",mochilaTeste.mochila.get(2).getDescricao());
        assertEquals("Flecha",mochilaTeste.mochila.get(3).getDescricao());
        assertEquals("Faca",mochilaTeste.mochila.get(4).getDescricao());
    }
    
    @Test
    public void ordenarItensFormaAscendenteItensQtdesIguaisDeveSerArcoEscudoEspadaFlecha(){
        Inventario mochilaTeste = new Inventario();
        
        mochilaTeste.adicionar(new Item("Arco",3));
        mochilaTeste.adicionar(new Item("Escudo",3));
        mochilaTeste.adicionar(new Item("Espada",3));
        mochilaTeste.adicionar(new Item("Flecha",3));

        mochilaTeste.ordenarItens();
    
        assertEquals("Arco",mochilaTeste.mochila.get(0).getDescricao());
        assertEquals("Escudo",mochilaTeste.mochila.get(1).getDescricao());
        assertEquals("Espada",mochilaTeste.mochila.get(2).getDescricao());
        assertEquals("Flecha",mochilaTeste.mochila.get(3).getDescricao());
    }
    
    @Test
    public void ordenarItensFormaDescendenteDeveSerMoedasBotasFlechaEscudoEspada(){
        Inventario mochilaTeste = new Inventario();
        
        mochilaTeste.adicionar(new Item("Flecha",3));
        mochilaTeste.adicionar(new Item("Moedas",8));
        mochilaTeste.adicionar(new Item("Espada",1));
        mochilaTeste.adicionar(new Item("Escudo",2));
        mochilaTeste.adicionar(new Item("Botas",4));

        mochilaTeste.ordenarItens(TipoOrdenacao.DESC);
    
        assertEquals("Moedas",mochilaTeste.mochila.get(0).getDescricao());
        assertEquals("Botas",mochilaTeste.mochila.get(1).getDescricao());
        assertEquals("Flecha",mochilaTeste.mochila.get(2).getDescricao());
        assertEquals("Escudo",mochilaTeste.mochila.get(3).getDescricao());
        assertEquals("Espada",mochilaTeste.mochila.get(4).getDescricao());
    }
    
    @Test
    public void ordenarItensFormaDescendenteArrayJaOrdenadoDeveSerMoedasFlechaEscudoEspada(){
        Inventario mochilaTeste = new Inventario();
        
        mochilaTeste.adicionar(new Item("Moedas",8));
        mochilaTeste.adicionar(new Item("Botas",4));
        mochilaTeste.adicionar(new Item("Flecha",3));
        mochilaTeste.adicionar(new Item("Escudo",2));
        mochilaTeste.adicionar(new Item("Espada",1));

        mochilaTeste.ordenarItens(TipoOrdenacao.DESC);
    
        assertEquals("Moedas",mochilaTeste.mochila.get(0).getDescricao());
        assertEquals("Botas",mochilaTeste.mochila.get(1).getDescricao());
        assertEquals("Flecha",mochilaTeste.mochila.get(2).getDescricao());
        assertEquals("Escudo",mochilaTeste.mochila.get(3).getDescricao());
        assertEquals("Espada",mochilaTeste.mochila.get(4).getDescricao());
    }
    
    @Test(expected=IndexOutOfBoundsException.class)
    public void ordenarItensFormaDescendenteArrayVazioDeveSerNull(){
        Inventario mochilaTeste = new Inventario();

        mochilaTeste.ordenarItens(TipoOrdenacao.DESC);
    
        assertEquals(null,mochilaTeste.mochila.get(0));
    }
    
    @Test(expected=IndexOutOfBoundsException.class)
    public void ordenarItensFormaAscendenteArrayVazioDeveSerNull(){
        Inventario mochilaTeste = new Inventario();

        mochilaTeste.ordenarItens();
    
        assertEquals(null,mochilaTeste.mochila.get(0));
    }
}