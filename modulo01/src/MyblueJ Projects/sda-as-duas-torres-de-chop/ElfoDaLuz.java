import java.util.ArrayList;
import java.util.Arrays;

public class ElfoDaLuz extends Elfo{
    private final ArrayList<String> NAO_PODE_PERDER = new ArrayList<>(Arrays.asList(
            "Espada de Galvorn"
        ));
    private final double CURA = 10.0;
    private int qtdeDeAtaques = 0;
    
    {
        this.dano = 21;
        this.ganharItem(new ItemImperdivel("Espada de Galvorn",1));    
    }
    
    //construtor
    public ElfoDaLuz(String nomeInformado) {
        super(nomeInformado);
    }
        
    public void perderItem(Item item){
        boolean podePerder = !NAO_PODE_PERDER.contains(item.getDescricao());
        if (podePerder) {
            super.perderItem(item);
        }
    }
    
    public void atacarComEspada(Dwarf inimigo){ 
        if (inimigo.getStatus()==Status.VIVO && this.status == Status.VIVO) {
            inimigo.perderVida();
            this.qtdeDeAtaques++;
            
            if(this.qtdeDeAtaques % 2 == 0)    
                this.ganharVida();
            else 
                this.perderVida();
            
            if(this.status!=Status.MORTO) this.experiencia += ganhoExp;
        }     
    }
    
    private void ganharVida(){
        this.setVida(this.vida+CURA);
    }
}