import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Arrays;

public class ElfoDaLuzTest {
    private final double DELTA = 0.1;
    
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void testaSeJaNasceuComArcoFlechaEspadaGalvornNoInventario(){
        Personagem feanor = new ElfoDaLuz("Feanor");
        
        ArrayList <Item> esperado = new ArrayList(Arrays.asList
                    (new Item("Arco",1),new Item("Flecha",12),new Item("Espada de Galvorn",1)));
        
        assertEquals(esperado,feanor.inventario.getItens());
    }
    
    @Test
    public void feanorAtacaComEspadaTresVezesVidaDeveSer68(){
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        Personagem bombur = new Dwarf("Bombur");
        
        feanor.atacarComEspada((Dwarf)bombur);
        feanor.atacarComEspada((Dwarf)bombur);
        feanor.atacarComEspada((Dwarf)bombur);
    
        assertEquals(68.0,feanor.getVida(),DELTA);
    }
    
    @Test
    public void feanorAtacaComEspadaDezesseteVezesStatusDeveSerMorto(){
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        
        for(int i=0; i<17; i++){
            feanor.atacarComEspada(new Dwarf("Bombur"));
        }
    
        assertEquals(Status.MORTO,feanor.getStatus());
    }
 
    @Test
    public void feanorAtacaComEspadaDezesseteVezesExpDeveSer16(){
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        
        for(int i=0; i<17; i++){
            feanor.atacarComEspada(new Dwarf("Bombur"));
        }
    
        assertEquals(16,feanor.getExperiencia());
    }
    
    @Test
    public void elfoDaLuzTentaPerderEspadaDeGalvornInventarioDeveTerArcoFlechaEspadaGalvorn(){
        Personagem feanor = new ElfoDaLuz("Feanor");
        
        Item espada = new Item("Espada de Galvorn",1);
    
        feanor.perderItem(espada);
        
        ArrayList <Item> esperado = new ArrayList(Arrays.asList
                    (new Item("Arco",1),new Item("Flecha",12),espada));
        
        assertEquals(esperado,feanor.inventario.getItens());
    }
    
    @Test
    public void elfoDaLuzAtacaDuasVezesDevePerderSomente11DeVida(){
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        
        feanor.atacarComEspada(new Dwarf("Bhalin"));
        feanor.atacarComEspada(new Dwarf("Bifur"));
        
        assertEquals(89,feanor.getVida(),DELTA);
    }    
}