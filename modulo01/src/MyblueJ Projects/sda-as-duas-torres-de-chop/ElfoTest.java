import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest {
    private final double DELTA = 0.1;
    
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void celebornAtiraUmaFlecha() {
        Dwarf anao = new Dwarf("Balin");
        Elfo celeborn = new Elfo("Celeborn");
        Item flecha = celeborn.inventario.buscar("Flecha");
        
        celeborn.atirarFlecha(anao);
        
        assertEquals(11, flecha.getQuantidade());
        assertEquals(Status.VIVO,anao.getStatus());
    }

    @Test
    public void legolasTentaAtirarEmAnaoMorto() {
        System.gc();
        System.runFinalization();
        
        Elfo elfo = new Elfo("Legolas");
        Dwarf anao = new Dwarf("Balin");
        Item flecha = elfo.inventario.buscar("Flecha");
        
        for(int i=0; i<12; i++){
            elfo.atirarFlecha(anao);
        }
        
        assertEquals(1, flecha.getQuantidade());
    }
    
    @Test
    public void anaoRecebeTresFlechadasDevePerder30DeVida() {
        System.gc();
        System.runFinalization();
        
        Elfo elfo = new Elfo("Legolas");
        Dwarf anao = new Dwarf("Balin");
        
        for(int i=0; i<3; i++){
            elfo.atirarFlecha(anao);
        }
        
        assertEquals(80.0, anao.getVida(),DELTA);
    }
    
    @Test
    public void elfoMataAnaoDeveAumentar11ExpAndStatusAnaoSerMorto(){
        Elfo elfo = new Elfo("Legolas");
        Dwarf anao = new Dwarf("Balin");
        
        for(int i=0; i<11; i++){
            elfo.atirarFlecha(anao);
        }        
        
        assertEquals(11, elfo.getExperiencia());
        assertEquals(Status.MORTO,anao.getStatus());
    }  
    
}


