public abstract class Personagem {
    protected String nome;
    protected double vida;    
    protected Status status;
    protected Inventario inventario;
    protected double dano;
    
    {
        this.dano = 0;
    }
    
    //construtores
    protected Personagem(String nome, double vida, Status status){
        this.nome = nome;
        this.vida = vida;    
        this.status = status;
        this.inventario = new Inventario();
    }
    
    //getters
    public String getNome(){
        return this.nome;
    }
    
    public double getVida(){
        return this.vida;
    }
    
    public Status getStatus(){
        return this.status;
    }
    
    public Inventario getInventario(){
        return this.inventario;
    }
    
    //setters
    public void setVida(double vida){
        this.vida = vida;
        if(this.vida<=0) setStatus(Status.MORTO);
    }
    
    private void setStatus(Status status){
        this.status = status;
    }
    
    public void ganharItem(Item itemAdquirido){
        inventario.adicionar(itemAdquirido);   
    }
    
    public void perderItem(Item itemPerdido){
        inventario.mochila.remove(itemPerdido);           
    }
    
    public void perderVida(){
        if(this.vida > 0.0) this.setVida(this.vida-dano);
        if(this.vida<=0.0) {
            this.setVida(0.0);
            this.setStatus(Status.MORTO);
        }
    }
}
