import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest {
    
    private final double DELTA = 0.1;
    
    @Test
    public void dwarfLevandoDuasFlechadasDeveTer90DeVida(){
        Dwarf anao = new Dwarf("Gimli");
        anao.perderVida();
        anao.perderVida();
        assertEquals(90.0,anao.getVida(),DELTA);
    }
    
    @Test
    public void dwarfNasceComStatusVivo(){
        Dwarf anao = new Dwarf("Gimli");
        assertEquals(Status.VIVO, anao.getStatus());
    }
}
