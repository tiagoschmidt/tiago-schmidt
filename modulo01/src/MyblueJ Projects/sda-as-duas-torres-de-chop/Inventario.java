import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Inventario {
   ArrayList <Item> mochila = new ArrayList<>();
   int itensNaMochila = 0;
   
   //construtores
   public Inventario(){
       
   }
   
   //getters
   public ArrayList <Item> getItens(){
       return mochila;
   }
   
   public StringBuilder getDescricoesItens(){
       StringBuilder descricoes = new StringBuilder();
       
       if(!mochila.isEmpty()){
           for (int i = 0; i < itensNaMochila; i++) {
               if (this.mochila.get(i) != null) {
                    String descricao = this.mochila.get(i).getDescricao();
                    
                    descricoes.append(descricao);
                    
                    boolean deveColocarVirgula = i != this.itensNaMochila-1;
                    
                    if (deveColocarVirgula) descricoes.append(",");
               }
           }      
       }
       
       return descricoes;
   }

   public int getItensNaMochila(){
       return this.itensNaMochila;
   }
   
   //adiciona novo item
   public void adicionar(Item novoItem){
       if(novoItem.getQuantidade() > 0){
           mochila.add(novoItem);
           itensNaMochila++;
       }
   }
   
   //remove um item informando a posicao
   public void remover(int posicao){
       mochila.remove(posicao);
       itensNaMochila--;
   }
   
   //retorna item da posicao desejada
   public Item obter(int posicao){
       return mochila.get(posicao);
   }
  
   //retorna o item que possui a maior quantidade
   public Item maiorQuantidade(){
       int maiorQtdeEncontradaParcial = 0;
       int posicao = 0;
       
       if(!mochila.isEmpty()){
           for (int i = 0; i < itensNaMochila; i++) {
               if (mochila.get(i).getQuantidade() > maiorQtdeEncontradaParcial){
                   maiorQtdeEncontradaParcial = mochila.get(i).getQuantidade();
                   posicao = i;
               }
           }
       }
       
       return mochila.get(posicao);
   }
   
   //buscar retorna o item com a descrição informada
   public Item buscar(String descricaoItem){
       
       if(!mochila.isEmpty()){
           for (int i = 0; i < itensNaMochila; i++) {
               if(mochila.get(i).getDescricao().equals(descricaoItem)) {
                   return mochila.get(i);
               }              
           }
       }
       
       return null;
   }
   
   //retorna uma lista invertida dos itens na mochila
   public ArrayList<Item> inverter(){
       ArrayList <Item> listaInvertida = new ArrayList <>(this.mochila.size());
   
       if(!mochila.isEmpty()){
           for (int i = itensNaMochila-1; i >= 0; i--) {
               listaInvertida.add(mochila.get(i));                
           }
       } else return null;
       
       return listaInvertida;
   }
   
   //ordena os itens por quantidade dos itens de forma ascendente
   public void ordenarItens() {
        ordenarItens(TipoOrdenacao.ASC);
    }

   public void ordenarItens(TipoOrdenacao tipoOrdenacao) {
       Collections.sort(this.mochila, new Comparator<Item>(){
            public int compare(Item item1, Item item2) {
                   int quantidade1 = item1.getQuantidade();
                   int quantidade2 = item2.getQuantidade();
                   return tipoOrdenacao == TipoOrdenacao.ASC ?
                   Integer.compare(quantidade1, quantidade2) :
                   Integer.compare(quantidade2, quantidade1);
            }
       });
   }
}