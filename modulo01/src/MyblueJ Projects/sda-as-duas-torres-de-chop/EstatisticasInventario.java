public class EstatisticasInventario {
    Inventario inventario;
    
    {
        inventario = new Inventario();
    }
    
    //construtor recebe um inventario como parametro
    public EstatisticasInventario(Inventario inventario){
        this.inventario = inventario;
    }
    
    //retorna media das quantidades de itens do inventario
    public double calcularMedia(){
        double resultadoMedia = 0.0;
        double somaQtdes = 0.0;
        
        if(!inventario.mochila.isEmpty()){
            
            if(inventario.itensNaMochila > 1){
                for(int i = 0; i < inventario.itensNaMochila; i++){
                    somaQtdes += inventario.mochila.get(i).getQuantidade();
                }
                
                resultadoMedia = somaQtdes / inventario.itensNaMochila;
                
            } else return inventario.mochila.get(0).getQuantidade();
        }        
        
        return resultadoMedia;
    }
    
    //retorna a mediana das quantidades de itens do inventario
    public double calcularMediana(){
        double resultadoMediana = 0.0;
        
        if(!inventario.mochila.isEmpty()){
            if(inventario.itensNaMochila > 1){
                inventario.ordenarItens();
                if(inventario.itensNaMochila % 2 == 0)
                    resultadoMediana = (inventario.mochila.get( inventario.itensNaMochila / 2 -1 ).getQuantidade() 
                        + inventario.mochila.get( inventario.itensNaMochila / 2 ).getQuantidade()) / 2.0;
                
                else resultadoMediana = inventario.mochila.get( inventario.itensNaMochila / 2 ).getQuantidade();        
                        
            } else return inventario.mochila.get(0).getQuantidade();     
        }
        
        return resultadoMediana;
    }
    
    //retorna quantidade de itens que possuem quantidade acimda da media
    public int qtdItensAcimaDaMedia(){
        int itensAcimaDaMedia = 0;
        
        if(!inventario.mochila.isEmpty() && inventario.itensNaMochila > 1){
            double resultadoMedia = calcularMedia();
            for(int i=0; i < inventario.mochila.size(); i++){
                if(inventario.mochila.get(i).getQuantidade() > resultadoMedia) itensAcimaDaMedia++;
            }
        }
    
        return itensAcimaDaMedia;
    }
}
