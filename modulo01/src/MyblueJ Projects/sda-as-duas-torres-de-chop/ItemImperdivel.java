import java.util.*;

public class ItemImperdivel extends Item {
    
    public ItemImperdivel(String descricao, int quantidade){
        super(descricao,quantidade);
    }
    
    public void setQuantidade(int novaQuantidade){
        boolean podeAlterar = novaQuantidade > 0;
        if(podeAlterar){
            this.quantidade = novaQuantidade;
        } else{
            this.quantidade = 1;
        }
    }
}