import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Arrays;

public class PersonagemTest{
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void elfoGanhaUmItemInventarioDoElfoDeveTerArcoFlechaEscudo(){
        Personagem elfo = new Elfo("Legolas");
        
        Item arco = new Item("Arco",1);
        Item flecha = new Item("Flecha",12);
        Item escudo = new Item("Escudo",1);
        
        elfo.ganharItem(escudo);
        
        ArrayList <Item> esperado = new ArrayList(Arrays.asList(arco,flecha,escudo));
        
        assertEquals(esperado,elfo.inventario.getItens());
    }
    
    @Test
    public void dwarfGanhaTresItensInventarioDoDwarfDeveTerMachadoEscudoArmadura(){
        Personagem anao = new Dwarf("Thurin");
        
        Item machado = new Item("Machado",2);
        Item escudo = new Item("Escudo",1);
        Item armadura = new Item("Armadura",4);
        
        anao.ganharItem(machado);
        anao.ganharItem(escudo);
        anao.ganharItem(armadura);
        
        ArrayList <Item> esperado = new ArrayList(Arrays.asList(machado,escudo,armadura));
        
        assertEquals(esperado,anao.inventario.getItens());
    }
    
    @Test
    public void elfoPerdeUmItemInventarioDoElfoDeveTerFlecha(){
        Personagem elfo = new Elfo("Legolas");
        
        Item arco = new Item("Arco",1);
        Item flecha = new Item("Flecha",12);
        
        elfo.perderItem(arco);
        
        ArrayList <Item> esperado = new ArrayList(Arrays.asList(arco,flecha));
        
        esperado.remove(arco);
        
        assertEquals(esperado,elfo.inventario.getItens());
    }
    
}
