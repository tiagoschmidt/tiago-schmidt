import java.util.*;

public class ExercitoElfos{
    private HashMap <Status, ArrayList<Elfo>> elfosPorStatus = new HashMap<>();
    private static final ArrayList<Class> TIPOS_ELFOS_PERMITIDOS = new ArrayList <>(
        Arrays.asList(
            ElfoVerde.class,
            ElfoNoturno.class
        ));
    Status vivo  = Status.VIVO;
    Status morto = Status.MORTO;
    
    public void alistarElfo(Elfo elfo){
        boolean podeAlistar = TIPOS_ELFOS_PERMITIDOS.contains(elfo.getClass());
        if(podeAlistar){
            ArrayList <Elfo> listaAtualElfos = this.elfosPorStatus.get(vivo);
            
            boolean inicializarLista = listaAtualElfos == null;
            if(inicializarLista){
                listaAtualElfos = new ArrayList<>();
                elfosPorStatus.put(vivo, listaAtualElfos);
            }
            
            listaAtualElfos.add(elfo);            
        }    
    }
    
    public ArrayList<Elfo> elfosAlistados(Status status){
        atualizarStatusDosElfos();
        return elfosPorStatus.get(status);
    }
    
    public void atualizarStatusDosElfos(){
        ArrayList <Elfo> listaAtualElfosVivos  = elfosPorStatus.get(vivo);
        boolean alguemMorreu = false;
        
        boolean peloMenosUmFoiAlistado = listaAtualElfosVivos != null;
        if(peloMenosUmFoiAlistado){
            ArrayList <Elfo> listaAtualizadaVivos  = new ArrayList<>();
            ArrayList <Elfo> listaAtualizadaMortos = new ArrayList<>();
           
            for(int i=0; i < listaAtualElfosVivos.size(); i++){
                if(listaAtualElfosVivos.get(i).getStatus() == morto){
                    listaAtualizadaMortos.add(listaAtualElfosVivos.get(i));
                    alguemMorreu = true;
                }else
                    listaAtualizadaVivos.add(listaAtualElfosVivos.get(i));
            }
            
            if(alguemMorreu){
                elfosPorStatus.put(morto, listaAtualizadaMortos); 
                elfosPorStatus.put(vivo, listaAtualizadaVivos); 
            }
        }
    }        
}