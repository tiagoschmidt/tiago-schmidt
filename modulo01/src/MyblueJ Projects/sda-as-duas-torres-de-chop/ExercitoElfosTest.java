import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class ExercitoElfosTest {
    Status vivo = Status.VIVO;
    Status morto = Status.MORTO;
    
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void alistarUmElfoNoturnoExercitoDeveTerUmElfoVivo(){
        ExercitoElfos exercito = new ExercitoElfos();
        Elfo faenor = new ElfoNoturno("Faenor");
        
        exercito.alistarElfo(faenor);
        
        assertEquals(1,exercito.elfosAlistados(vivo).size());
    }
    
    @Test
    public void alistarDoisElfosComunsExercitoDeveEstarVazio(){
        ExercitoElfos exercito = new ExercitoElfos();
        Elfo faenor = new Elfo("Faenor");
        Elfo legolas = new Elfo("Legolas");
        
        exercito.alistarElfo(faenor);
        exercito.alistarElfo(legolas);
        
        assertEquals(null,exercito.elfosAlistados(vivo));
    }
    
    @Test
    public void nenhumElfoAlistadoExercitoDeveEstarVazio(){
        ExercitoElfos exercito = new ExercitoElfos();
        
        assertEquals(null,exercito.elfosAlistados(vivo));   
    }
    
    @Test
    public void alistarCincoElfosNoturnosDoisDelesMorremExercitoDeveTerTresElfosVivosAndDoisMortos(){
        ExercitoElfos exercito = new ExercitoElfos();
        Elfo nightElf1 = new ElfoNoturno("Kinuth");
        Elfo nightElf2 = new ElfoNoturno("Ollendae");
        Elfo nightElf3 = new ElfoNoturno("Ivum");
        Elfo nightElf4 = new ElfoNoturno("Gyrem");
        Elfo nightElf5 = new ElfoNoturno("Othon");
        
        exercito.alistarElfo(nightElf1);
        exercito.alistarElfo(nightElf2);
        exercito.alistarElfo(nightElf3);
        exercito.alistarElfo(nightElf4);
        exercito.alistarElfo(nightElf5);
        
        for(int i=0; i<7; i++){
            nightElf2.atirarFlecha(new Dwarf("Coitado"));
            nightElf4.atirarFlecha(new Dwarf("Coitado"));
        }
        
        assertEquals(3,exercito.elfosAlistados(vivo).size());  
        assertEquals(2,exercito.elfosAlistados(morto).size()); 
    }
}
