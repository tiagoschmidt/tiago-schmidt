import java.util.Random;

public class DadoD6 implements Sorteador{
    public int sortear(){
        Random gerador = new Random();
        
        int numeroSorteado = gerador.nextInt(6)+1;
        
        return numeroSorteado;
    }    
}
