import java.util.ArrayList;
import java.util.Arrays;

public class ElfoVerde extends Elfo{
    private final ArrayList<String> ITENS_PERMITIDOS = new ArrayList<>(Arrays.asList(
            "Espada de Aço Valiriano",
            "Arco de Vidro",
            "Flecha de Vidro"
        ));
    
    {
        this.ganhoExp *= 2;    
    }
    
    //construtor
    public ElfoVerde(String nomeInformado) {
        super(nomeInformado);
    }
    
    public void ganharItem(Item item) {
        boolean descricaoValida = ITENS_PERMITIDOS.contains(item.getDescricao());
        if (descricaoValida) {
            super.ganharItem(item);
        }
    }
    
    public void perderItem(Item item){
        boolean descricaoValida = ITENS_PERMITIDOS.contains(item.getDescricao());
        if (descricaoValida) {
            super.perderItem(item);
        }
    }
}