import java.util.*;

public class NoturnosPorUltimo implements Estrategias{
    
    public List<Elfo> getOrdemDeAtaque(List<Elfo> soldados){
        List<Elfo> soldadosInstruidos = new ArrayList<>();
        
        for(int i=0; i < soldados.size(); i++){
            if(soldados.get(i).getClass() == ElfoVerde.class)
                soldadosInstruidos.add(0,soldados.get(i));
            else 
                soldadosInstruidos.add(soldados.get(i));
        }
        
        return soldadosInstruidos;
    }
    
}
