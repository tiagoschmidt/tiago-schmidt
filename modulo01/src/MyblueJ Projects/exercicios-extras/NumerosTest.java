import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class NumerosTest {
    @Test
    public void calculoDaMediaDeveSer20403045(){
        double[] entrada = new double[]{ 1.0,3.0,5.0,1.0,-10.0 };
        double[] resultadoEsperado = new double[]{2.0,4.0,3.0,-4.5};
                
        Numeros numeros = new Numeros(entrada);
        
        assertArrayEquals(resultadoEsperado,numeros.calcularMediaSeguinte(),0.1);
    }
    
    @Test
    public void calculoDaMediaDeveSer457012550(){
        double[] entrada = new double[]{8.0,1.0,-15.0,-10.0,20.0};
        double[] resultadoEsperado = new double[]{4.5,-7.0,-12.5,5.0};
                
        Numeros numeros = new Numeros(entrada);
        
        assertArrayEquals(resultadoEsperado,numeros.calcularMediaSeguinte(),0.1);
    }
    
    @Test
    public void calculoDaMediaDasPosicoes0e3NaoDeveSer55145(){
        double[] entrada = new double[]{4.0,3.0,5.0,8.0,20.0};
        double[] resultadoNaoEsperado = new double[]{5.5,4.0,6.5,14.5};       
        
        Numeros numeros = new Numeros(entrada);
        double[] resultadosCalculo = numeros.calcularMediaSeguinte(); 
        
        assertNotEquals(resultadoNaoEsperado[0],resultadosCalculo[0],0.1);
        assertNotEquals(resultadoNaoEsperado[3],resultadosCalculo[3],0.1);
    }
    
    @Test
    public void testaArrayComApenasUmNumero(){
        double[] entrada = new double[]{4.0};      
        
        Numeros numeros = new Numeros(entrada);
        double[] resultadosCalculo = numeros.calcularMediaSeguinte(); 
        
        assertEquals(0,resultadosCalculo.length);
    }
}
