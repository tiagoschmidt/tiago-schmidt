import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class AgendaContatosTest
{
    @Test
    public void consultarTelefoneDoGandalfDeveSer444444(){
        AgendaContatos agenda = new AgendaContatos();
        
        agenda.adicionar("Mithrandir", "444444");
        
        assertEquals("444444", agenda.consultar("Mithrandir"));
    }
    
    @Test
    public void consultarPeloTelefone4444444NomeDoContatoDeveSerGandalf(){
        AgendaContatos agenda = new AgendaContatos();
        
        agenda.adicionar("Mithrandir", "444444");
        
        assertEquals("Mithrandir", agenda.consultarPorTelefone("444444"));
    }
}