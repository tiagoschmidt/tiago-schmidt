import java.util.*;
public class AgendaContatos
{
    private LinkedHashMap<String, String> contatos = new LinkedHashMap<>();
    
    public void adicionar(String nome, String telefone){
        contatos.put(nome, telefone);
    }
    
    public String consultar(String nome){
        return contatos.get(nome);
    }
    
    public String consultarPorTelefone(String telefoneRecebido){
        String chaveContato; 
        String valorTelefone;
        
        for (HashMap.Entry<String, String> parEntrada : contatos.entrySet()) {
            chaveContato = parEntrada.getKey();
            
            valorTelefone = parEntrada.getValue();
            
            if(valorTelefone.equals(telefoneRecebido))
                return chaveContato;
        }
        
        return null;
    }
    
    //retorna csv de todos os nomes dos contatos e os telefones.
    public StringBuilder csv(){
        StringBuilder csv = new StringBuilder();
        
        String chave;
        String valor;
        String separador = System.lineSeparator();        
        
        for (HashMap.Entry<String, String> entry : contatos.entrySet()) {
            chave = entry.getKey();
            
            valor = entry.getValue();
            
            String contato = String.format("%s,%s%s",chave,valor,separador);
            csv.append(contato);
        }
        
        return csv;
    }
}