public class Numeros {
    double numeros[];
    
    public Numeros(double numeros[]){
        this.numeros = numeros;    
    }
    
    public double[] calcularMediaSeguinte(){
        double resultados[] = new double[this.numeros.length-1];
        double mediaSeguinte = 0.0;
        
        if(this.numeros.length>1){
            for(int i=0; i<numeros.length-1; i++){
                mediaSeguinte = (numeros[i] + numeros[i+1]) / 2;
                resultados[i] = mediaSeguinte;
            }        
        }
        
        return resultados;
    }    
}
