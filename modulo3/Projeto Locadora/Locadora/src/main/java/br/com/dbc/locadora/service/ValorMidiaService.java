/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.service;

import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.repository.ValorMidiaRepository;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author tiago.schmidt
 */
@Service
public class ValorMidiaService extends AbstractCrudService<ValorMidia, Long>{
    
    @Autowired
    private ValorMidiaRepository valorMidiaRepository;
    
    @Override
    protected JpaRepository<ValorMidia, Long> getRepository() {
        return valorMidiaRepository;
    }
    @Autowired
    private LocalDateService localDateService;
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void savePreco(Midia midia, Double valor){
        
        valorMidiaRepository.save(ValorMidia.builder()
                .valor(valor)
                .inicioVigencia(localDateService.now())
                .finalVigencia(null)
                .midia(midia)
        .build());        
    }
    
    public List<ValorMidia> findByMidiaId(Long id){
        return valorMidiaRepository.findByMidiaId(id);
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void deleteByMidia(Midia midia) {
        valorMidiaRepository.deleteByMidia(midia);
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void updatePreco(Midia midia, Double valor) {
        
        List<ValorMidia> valoresDaMidia = valorMidiaRepository.findByMidiaId(midia.getId());
        
        ValorMidia valorAtual = valoresDaMidia.get(valoresDaMidia.size()-1);
        
        boolean deveAlterarValor = valorAtual.getValor() != valor;
        
        if(deveAlterarValor){
            valorAtual.setFinalVigencia(localDateService.now());
            savePreco(midia, valor);
        }
            
    }
    
}
