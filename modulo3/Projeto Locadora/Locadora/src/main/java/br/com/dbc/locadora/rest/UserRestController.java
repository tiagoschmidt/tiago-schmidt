/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.entity.User;
import br.com.dbc.locadora.repository.UserRepository;
import br.com.dbc.locadora.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author tiago.schmidt
 */
@RestController
@RequestMapping("/api/user")
@PreAuthorize("hasAuthority('ADMIN_USER')")
public class UserRestController extends AbstractRestController<User, Long, UserService>{

    @Autowired
    private UserService userService;
    
    @Autowired
    private UserRepository userRepository;
    
    @Override
    protected UserService getService() {
        return userService;
    }
    
    @Override
    @PostMapping    
    public  ResponseEntity<?> post(@RequestBody User input){
        return  ResponseEntity.ok(userService.create(input));
    }
    
    @PostMapping("/password")
    public  ResponseEntity<?> changePassword(@RequestBody User input){
        return  ResponseEntity.ok(userService.changePassword(input));
    }

    
}
