/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.entity.Cliente;
import br.com.dbc.locadora.service.ClienteService;
import br.com.dbc.locadora.service.CorreiosWsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author tiago.schmidt
 */
@RestController
@RequestMapping("/api/cliente")
@PreAuthorize("hasAuthority('ADMIN_USER')")
public class ClienteRestController extends AbstractRestController<Cliente, Long, ClienteService> {

    @Autowired
    private CorreiosWsService correiosWsService;

    @Autowired
    private ClienteService clienteService;

    @Override
    protected ClienteService getService() {
        return clienteService;
    }

    @GetMapping("/cep/{cep}")
    public ResponseEntity<?> consultaCEP(@PathVariable String cep) {
        return ResponseEntity.ok(correiosWsService.buscarPorCep(cep));
    }

}
