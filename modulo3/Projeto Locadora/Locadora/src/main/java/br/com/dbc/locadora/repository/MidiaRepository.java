/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.enumerated.Categoria;
import br.com.dbc.locadora.enumerated.MidiaType;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author tiago.schmidt
 */
public interface MidiaRepository extends JpaRepository<Midia, Long> {

    public long countByTipo(MidiaType tipo);
    
    public long countByTipoAndFilmeId(MidiaType tipo, Long id);
    
    public List<Midia> findByFilme (Filme filme);
    
    public List<Midia> findByFilmeId (Long id);
    
    public List<Midia> findByFilmeIdAndAluguelIsNull (Long id);
    
    public Midia findFirstByFilmeIdOrderByAluguelPrevisaoAsc(Long id);
    
    public List<Midia> findByAluguelPrevisaoEquals(LocalDate dataAtual);
    
    public List<Midia> findByFilmeIdAndTipo (Long id, MidiaType tipo);
    
    public List<Midia> findByFilmeIdAndTipoAndAluguelIsNull (Long id, MidiaType tipo);
    
    public Page<Midia> findByFilmeTituloContainingIgnoreCaseOrFilmeCategoriaOrFilmeLancamentoBetween(Pageable pageable, String titulo, Categoria categoria, LocalDate inicio, LocalDate fim);
    
}
