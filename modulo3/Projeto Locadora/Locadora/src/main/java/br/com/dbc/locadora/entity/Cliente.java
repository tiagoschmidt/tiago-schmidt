/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author tiago.schmidt
 */
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Cliente extends AbstractEntity<Long> implements Serializable {

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @NotNull
    @Size(min = 2, max = 50)
    @Column(name = "NOME")
    private String nome;
    
    @NotNull
    @Size(min = 9, max = 11)
    @Column(name = "TELEFONE")
    private String telefone;
    
    @NotNull
    @Size(min = 2, max = 50)
    private String rua;
    
    private Long numero;
    
    @NotNull
    @Size(min = 2, max = 50)
    private String bairro;
    
    @NotNull
    @Size(min = 2, max = 50)
    private String cidade;
    
    @NotNull
    @Size(min = 2, max = 50)
    private String estado;
}
