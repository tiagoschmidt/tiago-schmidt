/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.service;

import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author tiago.schmidt
 * @param <E>
 */
@AllArgsConstructor
@Transactional(readOnly = true)
public abstract class AbstractCrudService<E, ID> {
    
    protected abstract JpaRepository<E, ID> getRepository();      
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public E save (E e) {
        return getRepository().save(e);
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void delete(ID id) {
        getRepository().deleteById(id);
    }
    
    public Page<E> findAll (Pageable pageable) {
        return getRepository().findAll(pageable);
    }
    
    public Optional<E> findById(ID id) {
        return getRepository().findById(id);
    }
    
}