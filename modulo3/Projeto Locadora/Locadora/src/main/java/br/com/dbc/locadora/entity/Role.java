package br.com.dbc.locadora.entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author tiago.schmidt
 */
@Entity
@Data
@Table(name="app_role")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Role extends AbstractEntity<Long> implements Serializable{
    
    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name="role_name")
    private String roleName;
    
    @Column(name="description")
    private String description;
}
