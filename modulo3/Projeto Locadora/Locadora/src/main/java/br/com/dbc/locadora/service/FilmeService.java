/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.service;

import br.com.dbc.locadora.dto.CatalogoDTO;
import br.com.dbc.locadora.dto.FilmeCatalogoDTO;
import br.com.dbc.locadora.dto.FilmeDTO;
import br.com.dbc.locadora.dto.MidiaCatalogoDTO;
import br.com.dbc.locadora.dto.PrecoDTO;
import br.com.dbc.locadora.enumerated.Categoria;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.enumerated.MidiaType;
import br.com.dbc.locadora.repository.FilmeRepository;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author tiago.schmidt
 */
@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class FilmeService extends AbstractCrudService<Filme, Long> {

    @Autowired
    private MidiaService midiaService;

    @Autowired
    private ValorMidiaService valorMidiaService;

    @Autowired
    private FilmeRepository filmeRepository;

    @Override
    protected JpaRepository<Filme, Long> getRepository() {
        return filmeRepository;
    }
    
    public Long countByTipo(MidiaType tipo, Long id){
        return midiaService.countByTipoAndFilmeId(tipo, id);
    }

    public Page<CatalogoDTO> listarCatalogo(Pageable pageable, FilmeCatalogoDTO dto) {

        List<Filme> filmesEncontrados = filmeRepository.findByTituloIgnoreCaseContaining(dto.getTitulo());
        List<CatalogoDTO> listaCatalogos = new ArrayList<>();

        for (int i = 0; i < filmesEncontrados.size(); i++) {
            CatalogoDTO catalogo = new CatalogoDTO();
            catalogo.setFilme(filmesEncontrados.get(i));
            List<MidiaCatalogoDTO> midiasDoCatalogo = new ArrayList<>();
            List<List<Midia>> tipos = new ArrayList<>();
            List<Midia> tipoDeMidia;
            List<Midia> midias = midiaService.findByFilmeId(filmesEncontrados.get(i).getId());

            for (int j = 0; j < midias.size(); j++) {
                if (j == 0 || midias.get(j).getTipo() != midias.get(j - 1).getTipo()) {
                    tipoDeMidia = midiaService.findByFilmeIdAndTipo(filmesEncontrados.get(i).getId(), midias.get(j).getTipo());
                    tipos.add(tipoDeMidia);
                }
            }
            for (int k = 0; k < tipos.size(); k++) {
                MidiaCatalogoDTO midiaCatalogo = new MidiaCatalogoDTO();
                String disponibilidade;
                if (!midiaService.findByFilmeIdAndAluguelIsNull(filmesEncontrados.get(i).getId()).isEmpty()) {
                    disponibilidade = "disponivel";
                } else {
                    disponibilidade = midiaService.findFirstByFilmeIdOrderByAluguelPrevisaoAsc(
                            filmesEncontrados.get(i).getId())
                            .getAluguel().getPrevisao().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                }
                midiaCatalogo.setTipo(tipos.get(k).get(0).getTipo());
                midiaCatalogo.setDisponibilidade(disponibilidade);
                midiaCatalogo.setValor(valorMidiaService.findByMidiaId(tipos.get(k).get(0).getId())
                        .get(valorMidiaService.findByMidiaId(tipos.get(k).get(0).getId()).size() - 1)
                        .getValor());
                midiasDoCatalogo.add(midiaCatalogo);
            }
            catalogo.setMidias(midiasDoCatalogo);
            listaCatalogos.add(catalogo);
        }
        Page<CatalogoDTO> catalogoFilmes = new PageImpl<>(listaCatalogos, pageable, listaCatalogos.size());
        return catalogoFilmes;
    }

    public List<PrecoDTO> precosDoFilme(Long id) {

        List<Midia> midias = midiaService.findByFilmeId(id);

        List<PrecoDTO> listaPrecos = new ArrayList<>();

        for (int i = 0; i < midias.size(); i++) {
            List<ValorMidia> valoresDaMidia = valorMidiaService.findByMidiaId(midias.get(i).getId());
            for (int j = 0; j < valoresDaMidia.size(); j++) {
                listaPrecos.add(PrecoDTO.builder()
                        .id(valoresDaMidia.get(j).getId())
                        .tipo(midias.get(i).getTipo())
                        .valor(valoresDaMidia.get(j).getValor())
                        .inicioVigencia(valoresDaMidia.get(j).getInicioVigencia())
                        .finalVigencia(valoresDaMidia.get(j).getFinalVigencia())
                        .build());
            }
        }

        return listaPrecos;
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Optional<Filme> salvarComMidias(FilmeDTO dto) {

        Filme filme = filmeRepository.save(Filme.builder()
                .titulo(dto.getTitulo())
                .lancamento(dto.getLancamento())
                .categoria(dto.getCategoria())
                .build());

        dto.getMidia().forEach((midia) -> {
            midiaService.saveMidias(filme, midia);
        });

        return filmeRepository.findById(filme.getId());
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Optional<Filme> update(FilmeDTO dto) {

        Filme filme = filmeRepository.save(Filme.builder()
                .id(dto.getId())
                .titulo(dto.getTitulo())
                .lancamento(dto.getLancamento())
                .categoria(dto.getCategoria())
                .build());

        dto.getMidia().forEach(midia -> {
            midiaService.update(midia, filme);
        });

        return filmeRepository.findById(filme.getId());
    }

    public Page<Filme> search(
            Pageable pageable,
            Categoria categoria,
            String titulo,
            LocalDate inicio,
            LocalDate fim) {
        inicio = inicio == null ? LocalDate.MIN : inicio;
        fim = fim == null ? LocalDate.MIN : fim;
        return filmeRepository
                .findByCategoriaOrTituloIgnoreCaseContainingOrLancamentoBetween(
                        pageable,
                        categoria,
                        titulo,
                        inicio,
                        fim
                );
    }

}
