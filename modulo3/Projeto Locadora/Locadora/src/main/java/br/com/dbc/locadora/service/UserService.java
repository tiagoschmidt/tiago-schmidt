/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.service;

import br.com.dbc.locadora.entity.User;
import br.com.dbc.locadora.repository.RoleRepository;
import br.com.dbc.locadora.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author tiago.schmidt
 */
@Service
@Transactional(readOnly = true)
public class UserService extends AbstractCrudService<User, Long>{

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    
    @Override
    protected JpaRepository<User, Long> getRepository() {
        return userRepository;
    }
    
    @Transactional(readOnly = false)
    public User create(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRoles(roleRepository.findByRoleName("STANDARD_USER"));
        return userRepository.save(user);
    }
    
    @Transactional(readOnly = false)
    public User changePassword(User user) {
        User userCadastrado = userRepository.findByUsername(user.getUsername());
        if (!userCadastrado.getPassword().equals(user.getPassword())) {
            userCadastrado.setPassword(passwordEncoder.encode(user.getPassword()));
            return userRepository.save(userCadastrado);
        }
        else return userCadastrado;
    }

}
