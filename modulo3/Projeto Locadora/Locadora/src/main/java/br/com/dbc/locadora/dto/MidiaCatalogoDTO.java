/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.dto;

import br.com.dbc.locadora.enumerated.MidiaType;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author tiago.schmidt
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MidiaCatalogoDTO implements Serializable{
    
    private MidiaType tipo;
    
    private Double valor;
    
    private String disponibilidade;
    
}
