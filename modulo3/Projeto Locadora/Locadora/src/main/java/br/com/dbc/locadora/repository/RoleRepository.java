package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.entity.Role;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author tiago.schmidt
 */
public interface RoleRepository extends JpaRepository<Role, Long> {
    
    List<Role> findByRoleName(String roleName);
    
}
