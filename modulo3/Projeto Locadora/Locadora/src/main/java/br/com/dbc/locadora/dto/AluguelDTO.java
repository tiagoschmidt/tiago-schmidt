/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.dto;

import java.io.Serializable;
import java.util.ArrayList;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author tiago.schmidt
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AluguelDTO implements Serializable {
    
    private Long idCliente;
    
    private ArrayList<Long> midias;
    
}
