/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.dto;

import br.com.dbc.locadora.enumerated.MidiaType;
import java.io.Serializable;
import lombok.Builder;
import lombok.Data;

/**
 *
 * @author tiago.schmidt
 */
@Data
@Builder
public class MidiaDTO implements Serializable{
    
    private MidiaType tipo;
    
    private Integer quantidade;
    
    private Double valor;
    
}
