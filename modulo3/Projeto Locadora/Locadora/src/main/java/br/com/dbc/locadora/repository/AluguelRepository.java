/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.entity.Aluguel;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author tiago.schmidt
 */
public interface AluguelRepository extends JpaRepository<Aluguel, Long> {
    
    public List<Aluguel> findByPrevisaoEqualsAndDevolucaoIsNull(Pageable pageable, LocalDate dataAtual);
    
}
