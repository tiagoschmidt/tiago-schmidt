/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.dto.AluguelDTO;
import br.com.dbc.locadora.enumerated.Categoria;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.enumerated.MidiaType;
import br.com.dbc.locadora.service.FilmeService;
import br.com.dbc.locadora.service.MidiaService;
import java.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author tiago.schmidt
 */
@RestController
@RequestMapping("/api/midia")
@PreAuthorize("hasAuthority('ADMIN_USER')")
public class MidiaRestController extends AbstractRestController<Midia, Long, MidiaService>{

    @Autowired
    private MidiaService midiaService;

    @Override
    protected MidiaService getService() {
        return midiaService;
    }
    
    @GetMapping("/search")
    public ResponseEntity<Page<Midia>> search(
            Pageable pageable,
            @RequestParam(value = "categoria", required = false) Categoria categoria, 
            @RequestParam(value = "titulo", required = false) String titulo, 
            @RequestParam(value = "lancamentoIni", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate inicio,
            @RequestParam(value = "lancamentoFim", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate fim){        
        return ResponseEntity.ok(midiaService.search(pageable, titulo, categoria, inicio, fim));
    }
    
    @GetMapping("/count/{tipo}")
    public ResponseEntity<Long> get(@PathVariable MidiaType tipo){
        return ResponseEntity.ok(midiaService.countByTipo(tipo));
    }

}
