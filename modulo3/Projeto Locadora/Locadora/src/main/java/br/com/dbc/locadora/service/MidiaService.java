/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.service;

import br.com.dbc.locadora.dto.MidiaDTO;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.enumerated.Categoria;
import br.com.dbc.locadora.enumerated.MidiaType;
import br.com.dbc.locadora.repository.MidiaRepository;
import java.time.LocalDate;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author tiago.schmidt
 */
@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class MidiaService extends AbstractCrudService<Midia, Long>{
    
    @Autowired
    private ValorMidiaService valorMidiaService;
    
    @Autowired
    private MidiaRepository midiaRepository;
    
    @Override
    protected JpaRepository<Midia, Long> getRepository() {
        return midiaRepository;
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void saveMidias(Filme filme, MidiaDTO dto){
                
        for(int i = 0; i < dto.getQuantidade(); i ++){
            Midia midia = midiaRepository.save(
                    Midia.builder()
                    .tipo(dto.getTipo())
                    .aluguel(null)
                    .filme(filme)
            .build());
            valorMidiaService.savePreco(midia, dto.getValor());
        }
    }
    
    public long countByTipo(MidiaType tipo){
        return midiaRepository.countByTipo(tipo);
    }
    
    public long countByTipoAndFilmeId(MidiaType tipo, Long id){
        return midiaRepository.countByTipoAndFilmeId(tipo, id);
    }
    
    public List<Midia> findByFilme ( Filme filme ){
        return midiaRepository.findByFilme(filme);
    }
    
    public List<Midia> findByFilmeId(Long id) {
        return midiaRepository.findByFilmeId(id);
    }
    
    public List<Midia> findByFilmeIdAndAluguelIsNull(Long id) {
        return midiaRepository.findByFilmeIdAndAluguelIsNull(id);
    }
    
    public Midia findFirstByFilmeIdOrderByAluguelPrevisaoAsc(Long id) {
        return midiaRepository.findFirstByFilmeIdOrderByAluguelPrevisaoAsc(id);
    }
    
    public List<Midia> findByFilmeIdAndTipo ( Long id, MidiaType midia ){
        return midiaRepository.findByFilmeIdAndTipo(id, midia);
    }
    
    public List<Midia> findByFilmeIdAndTipoAndAluguelIsNull ( Long id, MidiaType midia ){
        return midiaRepository.findByFilmeIdAndTipoAndAluguelIsNull(id, midia);
    }
    
    public Page<Midia> search(
            Pageable pageable, String titulo, Categoria categoria, 
            LocalDate inicio, LocalDate fim){
        
        inicio = inicio == null ? LocalDate.MIN : inicio;
        fim = fim == null ? LocalDate.MIN : fim;
        
        return midiaRepository
                .findByFilmeTituloContainingIgnoreCaseOrFilmeCategoriaOrFilmeLancamentoBetween(
                        pageable, titulo, categoria, inicio, fim);
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void update(MidiaDTO dto, Filme filme){
        
        List<Midia> midiasAtuais = findByFilmeIdAndTipo(filme.getId(), dto.getTipo());
        
        boolean deveDeletar = midiasAtuais.size() >= dto.getQuantidade();
        
        if(deveDeletar){
            boolean tamanhosDiferentes = midiasAtuais.size() != dto.getQuantidade();
            if(tamanhosDiferentes)
                deletarMidias(midiasAtuais.size() - dto.getQuantidade(), midiasAtuais);
        }
        else{
            dto.setQuantidade(dto.getQuantidade() - midiasAtuais.size());
            saveMidias( filme, dto );
        }
        
        midiasAtuais = findByFilmeIdAndTipo(filme.getId(), dto.getTipo());
        
        midiasAtuais.forEach((midia) -> {
            valorMidiaService.updatePreco(midia, dto.getValor());
        });
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void deletarMidias(Integer qtde, List<Midia> midias){
        
        midias.stream()
                .filter(m->m.getAluguel() == null)
                .limit(qtde).forEach(m->{
            valorMidiaService.deleteByMidia(m);
            delete(m.getId());
        });
        
    }
}
