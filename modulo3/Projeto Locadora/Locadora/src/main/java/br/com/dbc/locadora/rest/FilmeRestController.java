/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.dto.CatalogoDTO;
import br.com.dbc.locadora.dto.FilmeCatalogoDTO;
import br.com.dbc.locadora.dto.FilmeDTO;
import br.com.dbc.locadora.dto.PrecoDTO;
import br.com.dbc.locadora.enumerated.Categoria;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.enumerated.MidiaType;
import br.com.dbc.locadora.service.FilmeService;
import java.time.LocalDate;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author tiago.schmidt
 */
@RestController
@RequestMapping("/api/filme")
@PreAuthorize("hasAuthority('ADMIN_USER')")
public class FilmeRestController extends AbstractRestController<Filme, Long, FilmeService>{

    @Autowired
    private FilmeService filmeService;
    
    @Override
    protected FilmeService getService() {
        return filmeService;
    }
    
    @PostMapping("/midia")
    public ResponseEntity<?> post(@RequestBody FilmeDTO input) {
        return filmeService.salvarComMidias(input).map(ResponseEntity::ok).orElseGet(()->ResponseEntity.notFound().build());
    }
    
    @PutMapping("/{id}/midia")
    public ResponseEntity<?> put(@PathVariable("id") Long id, @RequestBody FilmeDTO input) {
        return filmeService.update(input).map(ResponseEntity::ok).orElseGet(()->ResponseEntity.notFound().build());
    }
    
    @GetMapping("/search")
    public ResponseEntity<Page<Filme>> get(
            Pageable pageable,
            @RequestParam(value = "categoria", required = false) Categoria categoria, 
            @RequestParam(value = "titulo", required = false) String titulo, 
            @RequestParam(value = "lancamentoIni", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate inicio,
            @RequestParam(value = "lancamentoFim", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate fim){
        return ResponseEntity.ok(filmeService.search(pageable, categoria, titulo, inicio, fim));
    }
    
    @GetMapping("/precos/{id}")
    public List<PrecoDTO> listarPrecosFilme(@PathVariable Long id){
        return filmeService.precosDoFilme(id);
    }
        
    @GetMapping("/count/{id}/{tipo}")
    public ResponseEntity<Long> get(@PathVariable Long id, @PathVariable MidiaType tipo){
        return ResponseEntity.ok(filmeService.countByTipo(tipo, id));
    }
    
    @PostMapping("/search/catalogo")
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
    public ResponseEntity<Page<CatalogoDTO>> listarCatalogoFilmes(Pageable pageable, @RequestBody FilmeCatalogoDTO input){
        return ResponseEntity.ok(filmeService.listarCatalogo(pageable, input));
    }
}
