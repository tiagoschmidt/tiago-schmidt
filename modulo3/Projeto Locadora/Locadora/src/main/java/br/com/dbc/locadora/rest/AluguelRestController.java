/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.dto.AluguelDTO;
import br.com.dbc.locadora.dto.DevolucaoAluguelDTO;
import br.com.dbc.locadora.entity.Aluguel;
import br.com.dbc.locadora.service.AluguelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author tiago.schmidt
 */
@RestController
@RequestMapping("/api/aluguel")
@PreAuthorize("hasAuthority('ADMIN_USER')")
public class AluguelRestController extends AbstractRestController<Aluguel, Long, AluguelService>{

    @Autowired
    private AluguelService aluguelService;

    @Override
    protected AluguelService getService() {
        return aluguelService;
    }
    
    @PostMapping("/retirada")
    public ResponseEntity<?> alugarFilme(@RequestBody AluguelDTO input){
        return ResponseEntity.ok(aluguelService.alugarFilme(input));
    }
    
    @PostMapping("/devolucao")
    public ResponseEntity<?> devolverFilme(@RequestBody DevolucaoAluguelDTO input){
        return ResponseEntity.ok(aluguelService.devolverFilme(input));
    }
    
    @GetMapping("/devolucao")
    public ResponseEntity<?> listarDevolucoesHoje(Pageable pageable){
        return ResponseEntity.ok(aluguelService.listarDevolucoesHoje(pageable));
    }
}
