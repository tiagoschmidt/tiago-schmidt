/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.service;

import br.com.dbc.locadora.ws.ConsultaCEP;
import br.com.dbc.locadora.ws.ConsultaCEPResponse;
import br.com.dbc.locadora.ws.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import br.com.dbc.locadora.config.WsConnector;
import br.com.dbc.locadora.dto.CepDTO;
import javax.xml.bind.JAXBElement;
import org.springframework.stereotype.Service;

/**
 *
 * @author tiago.schmidt
 */
@Service
public class CorreiosWsService {

    private final String url = "https://apps.correios.com.br/SigepMasterJPA/AtendeClienteService/AtendeCliente";
    
    @Autowired
    private ObjectFactory objectFactory;

    @Autowired
    private WsConnector wsConnector;

    public CepDTO buscarPorCep (String cep) {
        
        ConsultaCEP busca = objectFactory.createConsultaCEP();
        
        busca.setCep(cep);
        
        ConsultaCEPResponse retorno = ((JAXBElement<ConsultaCEPResponse>) 
                wsConnector.callWebService(url, objectFactory.createConsultaCEP(busca))).getValue();
        
        return CepDTO.builder()
                .rua(retorno.getReturn().getEnd())
                .bairro(retorno.getReturn().getBairro())
                .cidade(retorno.getReturn().getCidade())
                .estado(retorno.getReturn().getUf())
                .build();        
    }

}
