/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.service;

import br.com.dbc.locadora.dto.AluguelDTO;
import br.com.dbc.locadora.dto.DevolucaoAluguelDTO;
import br.com.dbc.locadora.dto.RetornoAluguelDTO;
import br.com.dbc.locadora.entity.Aluguel;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.repository.AluguelRepository;
import br.com.dbc.locadora.repository.MidiaRepository;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author tiago.schmidt
 */
@Service
public class AluguelService extends AbstractCrudService<Aluguel, Long>{
    
    @Autowired
    private AluguelRepository aluguelRepository;
    
    @Autowired
    private MidiaRepository midiaRepository;
    
    @Autowired
    private ClienteService clienteService;
    
    @Autowired 
    private LocalDateService localDateService;
    
    @Autowired
    private MidiaService midiaService;
    
    @Autowired
    private ValorMidiaService valorMidiaService;
    
    @Override
    protected JpaRepository<Aluguel, Long> getRepository() {
        return aluguelRepository;
    }
    
    public Page<Filme> listarDevolucoesHoje(Pageable pageable){    
        
        List<Midia> midiasAlugueisHoje = midiaRepository
                    .findByAluguelPrevisaoEquals(LocalDate.now());
        List<Filme> filmesDevolucaoHoje = new ArrayList<>();        
        
        midiasAlugueisHoje.forEach(m->{
            if(!filmesDevolucaoHoje.contains(m.getFilme()))
                filmesDevolucaoHoje.add(m.getFilme());
        });            
        
        Page<Filme> filmesVoltandoHoje = new PageImpl<>(filmesDevolucaoHoje, pageable, filmesDevolucaoHoje.size());
        
        return filmesVoltandoHoje;
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public RetornoAluguelDTO alugarFilme(AluguelDTO dto) {
        
        Aluguel aluguel = save(Aluguel.builder()
                .retirada(localDateService.dateNow())
                .previsao(localDateService.dateNow().plusDays(dto.getMidias().size()))
                .devolucao(null)
                .multa(null)
                .cliente(clienteService.findById(dto.getIdCliente()).orElse(null))
                .build());
        
        RetornoAluguelDTO retirada = RetornoAluguelDTO.builder()
                .valorAluguel(0.0)
                .retirada(aluguel.getRetirada())
                .previsao(aluguel.getPrevisao())
                .devolucao(aluguel.getDevolucao())
                .build();
        
        for(int i = 0; i < dto.getMidias().size(); i++){
            Midia midia = midiaService.findById(dto.getMidias().get(i)).orElse(null);
            List<ValorMidia> valoresDaMidia = valorMidiaService.findByMidiaId(midia.getId());
            Double soma = retirada.getValorAluguel() + valoresDaMidia.get(valoresDaMidia.size()-1).getValor();
            retirada.setValorAluguel(soma);
            midia.setAluguel(aluguel);
        }
                
        return retirada;
    }
    
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public RetornoAluguelDTO devolverFilme(DevolucaoAluguelDTO dto) {
        
        RetornoAluguelDTO devolucao = new RetornoAluguelDTO();
        Aluguel aluguel = aluguelRepository.findById(midiaService.findById(dto.getMidias().get(0)).orElse(null).getAluguel().getId()).orElse(null);
        aluguel.setDevolucao(localDateService.now());
        devolucao.setValorAluguel(0.0);
        
        for(int i = 0; i < dto.getMidias().size(); i++){
            
            Midia midia = midiaService.findById(dto.getMidias().get(i)).orElse(null);            
            List<ValorMidia> valoresDaMidia = valorMidiaService.findByMidiaId(midia.getId());
            
            LocalDateTime horarioPrevisaoEntrega = aluguel.getPrevisao().atTime(16, 0, 0, 0);
            
            Double valorVigente = valoresDaMidia.get(valoresDaMidia.size()-1).getValor();
            
            if(localDateService.now().compareTo(horarioPrevisaoEntrega) > 0){ 
                if(aluguel.getMulta() == null)
                    aluguel.setMulta(0.0);
                aluguel.setMulta(aluguel.getMulta() + valorVigente);
            }                       
            devolucao.setValorAluguel(devolucao.getValorAluguel() + valorVigente);
            
            midia.setAluguel(null);
        }        
        
        Double totalAluguel = aluguel.getMulta() != null ? devolucao.getValorAluguel() + aluguel.getMulta() : devolucao.getValorAluguel();
        
        devolucao = RetornoAluguelDTO.builder()
                .valorAluguel(totalAluguel)
                .retirada(aluguel.getRetirada())
                .previsao(aluguel.getPrevisao())
                .devolucao(aluguel.getDevolucao())
                .build();
        
        return devolucao;
    }    
}
