/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.ValorMidia;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author tiago.schmidt
 */
public interface ValorMidiaRepository extends JpaRepository<ValorMidia, Long> {
    
    public List<ValorMidia> findByMidiaId(Long id);
    
    void deleteByMidia(Midia midia);
    
}
