/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.dto;

import br.com.dbc.locadora.enumerated.MidiaType;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author tiago.schmidt
 */
@Data
@Builder
public class PrecoDTO {
    
    private Long id;
    
    private MidiaType tipo;
    
    private double valor;
    
    @JsonFormat(pattern = "dd/MM/yyyy'T'HH:mm:ss", shape = JsonFormat.Shape.STRING)
    @DateTimeFormat(pattern = "dd/MM/yyyy'T'HH:mm:ss")
    private LocalDateTime inicioVigencia;
    
    @JsonFormat(pattern = "dd/MM/yyyy'T'HH:mm:ss", shape = JsonFormat.Shape.STRING)
    @DateTimeFormat(pattern = "dd/MM/yyyy'T'HH:mm:ss")
    private LocalDateTime finalVigencia;
    
}
