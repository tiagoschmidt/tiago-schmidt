/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.entity;

import br.com.dbc.locadora.enumerated.MidiaType;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author tiago.schmidt
 */
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Midia extends AbstractEntity<Long> implements Serializable {

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @NotNull
    @Column(name = "TIPO")
    @Enumerated
    private MidiaType tipo;
    
    @JoinColumn(name = "ID_ALUGUEL", referencedColumnName = "ID")
    @ManyToOne(optional = true, cascade = CascadeType.ALL)
    private Aluguel aluguel;
    
    @NotNull
    @JoinColumn(name = "ID_FILME", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Filme filme;
    
}
