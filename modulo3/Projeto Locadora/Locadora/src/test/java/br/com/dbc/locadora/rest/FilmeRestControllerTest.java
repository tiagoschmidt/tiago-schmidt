/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.LocadoraApplicationTests;
import br.com.dbc.locadora.dto.AluguelDTO;
import br.com.dbc.locadora.dto.CatalogoDTO;
import br.com.dbc.locadora.dto.FilmeCatalogoDTO;
import br.com.dbc.locadora.dto.FilmeDTO;
import br.com.dbc.locadora.dto.MidiaDTO;
import br.com.dbc.locadora.dto.PrecoDTO;
import br.com.dbc.locadora.entity.Cliente;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.enumerated.Categoria;
import br.com.dbc.locadora.enumerated.MidiaType;
import br.com.dbc.locadora.repository.FilmeRepository;
import br.com.dbc.locadora.repository.MidiaRepository;
import br.com.dbc.locadora.repository.ValorMidiaRepository;
import br.com.dbc.locadora.service.AluguelService;
import br.com.dbc.locadora.service.ClienteService;
import br.com.dbc.locadora.service.FilmeService;
import br.com.dbc.locadora.service.LocalDateService;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author tiago.schmidt
 */
public class FilmeRestControllerTest extends LocadoraApplicationTests {

    @MockBean
    private LocalDateService localDateService;

    @Autowired
    private FilmeRestController filmeRestController;

    @Autowired
    private FilmeService filmeService;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private AluguelService aluguelService;

    @Autowired
    private ValorMidiaRepository valorMidiaRepository;

    @Autowired
    private MidiaRepository midiaRepository;

    @Autowired
    private FilmeRepository filmeRepository;

    @Override
    protected AbstractRestController getController() {
        return filmeRestController;
    }

    @Before
    public void beforeTest() {
        super.setUp();
        Mockito.reset();
        valorMidiaRepository.deleteAll();
        midiaRepository.deleteAll();
        filmeRepository.deleteAll();
    }

    @After
    public void tearDown() {
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void filmeCreateTest() throws Exception {
        Filme filme = Filme.builder().titulo("rei arthur").categoria(Categoria.ACAO).lancamento(LocalDate.of(2018, 10, 22)).build();
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(filme)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(filme.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.categoria").value(filme.getCategoria().toString()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value(filme.getLancamento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))));
        List<Filme> filmes = filmeRepository.findAll();
        Assert.assertEquals(1, filmes.size());
        Assert.assertEquals(filme.getTitulo(), filmes.get(0).getTitulo());
        Assert.assertEquals(filme.getCategoria(), filmes.get(0).getCategoria());
        Assert.assertEquals(filme.getLancamento(), filmes.get(0).getLancamento());
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void filmeCreateWithMidiaTest() throws Exception {
        Mockito.when(localDateService.now()).thenReturn(LocalDateTime.of(2018, 07, 20, 17, 0, 0));

        ArrayList<MidiaDTO> dtoMidiaList = new ArrayList<>();
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.VHS).quantidade(5).valor(2.0).build());
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.DVD).quantidade(5).valor(3.0).build());
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.BLUE_RAY).quantidade(5).valor(4.0).build());
        FilmeDTO filmeDTO = FilmeDTO.builder()
                .titulo("rei arthur")
                .categoria(Categoria.ACAO)
                .lancamento(LocalDate.of(2018, 11, 02))
                .midia(dtoMidiaList)
                .build();
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/midia")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(filmeDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(filmeDTO.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.categoria").value(filmeDTO.getCategoria().toString()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value(filmeDTO.getLancamento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))));
        List<Filme> filmes = filmeRepository.findAll();
        List<Midia> midias = midiaRepository.findAll();
        List<ValorMidia> valores = valorMidiaRepository.findAll();
        Assert.assertEquals(1, filmes.size());
        Assert.assertEquals(15, midias.size());
        Assert.assertEquals(5, midiaRepository.countByTipoAndFilmeId(midias.get(0).getTipo(), filmes.get(0).getId()));
        Assert.assertEquals(15, valores.size());
        Assert.assertEquals(2.0, valores.get(0).getValor(), 0.0);
        Assert.assertEquals(midias.get(0).getId(), valores.get(0).getMidia().getId());
        Assert.assertEquals(LocalDateTime.of(2018, 07, 20, 17, 0, 0), valores.get(0).getInicioVigencia());
        Assert.assertEquals(null, valores.get(0).getFinalVigencia());
        Assert.assertEquals(filmeDTO.getTitulo(), filmes.get(0).getTitulo());
        Assert.assertEquals(filmeDTO.getCategoria(), filmes.get(0).getCategoria());
        Assert.assertEquals(filmeDTO.getLancamento(), filmes.get(0).getLancamento());
        Assert.assertEquals(dtoMidiaList.get(0).getTipo(), midias.get(0).getTipo());
        Assert.assertEquals(filmes.get(0).getId(), midias.get(0).getFilme().getId());
        Assert.assertEquals(Long.valueOf(dtoMidiaList.get(0).getQuantidade()), Long.valueOf(midiaRepository.countByTipo(dtoMidiaList.get(0).getTipo())));
        Assert.assertEquals(dtoMidiaList.get(0).getValor(), valorMidiaRepository.findByMidiaId(midias.get(0).getId()).get(0).getValor(), 0.0);

    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void filmeUpdateTest() throws Exception {
        Mockito.when(localDateService.now()).thenReturn(LocalDateTime.of(2018, 07, 20, 17, 0, 0));

        ArrayList<MidiaDTO> dtoMidiaList = new ArrayList<>();
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.VHS).quantidade(5).valor(2.0).build());
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.DVD).quantidade(5).valor(3.0).build());
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.BLUE_RAY).quantidade(5).valor(4.0).build());
        FilmeDTO filmeDTO = FilmeDTO.builder()
                .titulo("rei arthur")
                .categoria(Categoria.ACAO)
                .lancamento(LocalDate.of(2018, 11, 02))
                .midia(dtoMidiaList)
                .build();
        Filme filme = objectMapper.readValue(
                restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/midia")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsBytes(filmeDTO)))
                        .andExpect(MockMvcResultMatchers.status().isOk())
                        .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(filmeDTO.getTitulo()))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.categoria").value(filmeDTO.getCategoria().toString()))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value(filmeDTO.getLancamento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                        .andReturn().getResponse().getContentAsString(), Filme.class);

        ArrayList<MidiaDTO> dtoMidiaListUpdate = new ArrayList<>();
        dtoMidiaListUpdate.add(MidiaDTO.builder().tipo(MidiaType.VHS).quantidade(1).valor(3.0).build());
        dtoMidiaListUpdate.add(MidiaDTO.builder().tipo(MidiaType.DVD).quantidade(1).valor(5.0).build());
        dtoMidiaListUpdate.add(MidiaDTO.builder().tipo(MidiaType.BLUE_RAY).quantidade(1).valor(7.0).build());
        FilmeDTO filmeDtoUpdate = FilmeDTO.builder()
                .id(filme.getId())
                .titulo("cronicas de narnia")
                .categoria(Categoria.AVENTURA)
                .lancamento(LocalDate.of(2012, 12, 25))
                .midia(dtoMidiaListUpdate)
                .build();
        restMockMvc.perform(MockMvcRequestBuilders.put("/api/filme/{id}/midia", filme.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(filmeDtoUpdate)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(filme.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(filmeDtoUpdate.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.categoria").value(filmeDtoUpdate.getCategoria().toString()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value(filmeDtoUpdate.getLancamento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))));

        List<Filme> filmes = filmeRepository.findAll();
        List<Midia> midias = midiaRepository.findAll();
        List<ValorMidia> valores = valorMidiaRepository.findAll();
        Assert.assertEquals(1, filmes.size());
        Assert.assertEquals(3, midias.size());
        Assert.assertEquals(null, midias.get(0).getAluguel());
        Assert.assertEquals(1, midiaRepository.countByTipo(midias.get(0).getTipo()));
        Assert.assertEquals(6, valores.size());
        Assert.assertEquals(3.0, valores.get(1).getValor(), 0.0);
        Assert.assertEquals(midias.get(0).getId(), valores.get(3).getMidia().getId());
        Assert.assertEquals(LocalDateTime.of(2018, 07, 20, 17, 0, 0), valores.get(0).getInicioVigencia());
        Assert.assertEquals(LocalDateTime.of(2018, 07, 20, 17, 0, 0), valores.get(0).getFinalVigencia());
        Assert.assertEquals(LocalDateTime.of(2018, 07, 20, 17, 0, 0), valores.get(1).getInicioVigencia());
        Assert.assertEquals(null, valores.get(3).getFinalVigencia());
        Assert.assertEquals(filmeDtoUpdate.getTitulo(), filmes.get(0).getTitulo());
        Assert.assertEquals(filmeDtoUpdate.getCategoria(), filmes.get(0).getCategoria());
        Assert.assertEquals(filmeDtoUpdate.getLancamento(), filmes.get(0).getLancamento());
        Assert.assertEquals(dtoMidiaListUpdate.get(0).getTipo(), midias.get(0).getTipo());
        Assert.assertEquals(filmes.get(0).getId(), midias.get(0).getFilme().getId());
        Assert.assertEquals(Long.valueOf(dtoMidiaListUpdate.get(0).getQuantidade()), Long.valueOf(midiaRepository.countByTipo(dtoMidiaList.get(0).getTipo())));
        Assert.assertEquals(dtoMidiaListUpdate.get(0).getValor(), valorMidiaRepository.findByMidiaId(midias.get(0).getId()).get(1).getValor(), 0.0);

    }
    
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void filmeUpdateNotDeletingTest() throws Exception {
        Mockito.when(localDateService.now()).thenReturn(LocalDateTime.of(2018, 07, 20, 17, 0, 0));

        ArrayList<MidiaDTO> dtoMidiaList = new ArrayList<>();
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.VHS).quantidade(1).valor(2.0).build());
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.DVD).quantidade(1).valor(3.0).build());
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.BLUE_RAY).quantidade(1).valor(4.0).build());
        FilmeDTO filmeDTO = FilmeDTO.builder()
                .titulo("rei arthur")
                .categoria(Categoria.ACAO)
                .lancamento(LocalDate.of(2018, 11, 02))
                .midia(dtoMidiaList)
                .build();
        Filme filme = objectMapper.readValue(
                restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/midia")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsBytes(filmeDTO)))
                        .andExpect(MockMvcResultMatchers.status().isOk())
                        .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(filmeDTO.getTitulo()))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.categoria").value(filmeDTO.getCategoria().toString()))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value(filmeDTO.getLancamento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                        .andReturn().getResponse().getContentAsString(), Filme.class);
        ArrayList<MidiaDTO> dtoMidiaList2 = new ArrayList<>();
        dtoMidiaList2.add(MidiaDTO.builder().tipo(MidiaType.VHS).quantidade(2).valor(2.0).build());
        dtoMidiaList2.add(MidiaDTO.builder().tipo(MidiaType.DVD).quantidade(2).valor(3.0).build());
        dtoMidiaList2.add(MidiaDTO.builder().tipo(MidiaType.BLUE_RAY).quantidade(2).valor(4.0).build());
        FilmeDTO filmeDtoUpdate = FilmeDTO.builder()
                .id(filme.getId())
                .titulo("rei arthur")
                .categoria(Categoria.ACAO)
                .lancamento(LocalDate.of(2018, 11, 02))
                .midia(dtoMidiaList2)
                .build();
        filmeService.update(filmeDtoUpdate);
        List<Filme> filmes = filmeRepository.findAll();
        List<Midia> midias = midiaRepository.findAll();
        Assert.assertEquals(1, filmes.size());
        Assert.assertEquals(6, midias.size());

    }

    @Test(expected = Exception.class)
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void filmesExceptionsTest() throws Exception {
        FilmeDTO filmeComId = FilmeDTO.builder().id(1l).build();
        FilmeDTO filmeIdNull = FilmeDTO.builder().build();
        filmeIdNull.setId(null);
        restMockMvc.perform(MockMvcRequestBuilders.put("/api/filme/{id}/midia", filmeIdNull.getId()));
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/midia")
                .content(objectMapper.writeValueAsBytes(filmeComId)));
        restMockMvc.perform(MockMvcRequestBuilders.get("/api/filme/search"));
    }

    @Test()
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void filmeCatalogoTest() throws Exception {
        Mockito.when(localDateService.now()).thenReturn(LocalDateTime.of(2018, 07, 20, 17, 0, 0));

        ArrayList<MidiaDTO> dtoMidiaList = new ArrayList<>();
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.VHS).quantidade(5).valor(2.0).build());
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.DVD).quantidade(5).valor(3.0).build());
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.BLUE_RAY).quantidade(5).valor(4.0).build());
        FilmeDTO filmeDTO = FilmeDTO.builder()
                .titulo("rei arthur")
                .categoria(Categoria.ACAO)
                .lancamento(LocalDate.of(2018, 11, 02))
                .midia(dtoMidiaList)
                .build();
        Filme filme = filmeService.salvarComMidias(filmeDTO).orElse(null);

        ArrayList<MidiaDTO> dtoMidiaListUpdate = new ArrayList<>();
        dtoMidiaListUpdate.add(MidiaDTO.builder().tipo(MidiaType.VHS).quantidade(1).valor(3.0).build());
        dtoMidiaListUpdate.add(MidiaDTO.builder().tipo(MidiaType.DVD).quantidade(1).valor(5.0).build());
        dtoMidiaListUpdate.add(MidiaDTO.builder().tipo(MidiaType.BLUE_RAY).quantidade(1).valor(7.0).build());
        FilmeDTO filmeDtoUpdate = FilmeDTO.builder()
                .id(filme.getId())
                .titulo("cronicas de narnia")
                .categoria(Categoria.AVENTURA)
                .lancamento(LocalDate.of(2012, 12, 25))
                .midia(dtoMidiaListUpdate)
                .build();
        restMockMvc.perform(MockMvcRequestBuilders.put("/api/filme/{id}/midia", filme.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(filmeDtoUpdate)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(filme.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.titulo").value(filmeDtoUpdate.getTitulo()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.categoria").value(filmeDtoUpdate.getCategoria().toString()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lancamento").value(filmeDtoUpdate.getLancamento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))));

        Pageable pageable = Pageable.unpaged();
        FilmeCatalogoDTO dto = FilmeCatalogoDTO.builder().titulo("nic").build();
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/filme/search/catalogo?titulo=nic")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(filmeDtoUpdate)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$").isNotEmpty());
        List<CatalogoDTO> catalogo = filmeService.listarCatalogo(pageable, dto).getContent();

        Assert.assertEquals(1, catalogo.size());
        Assert.assertEquals(filmeDtoUpdate.getId(), catalogo.get(0).getFilme().getId());
        Assert.assertEquals("disponivel", catalogo.get(0).getMidias().get(0).getDisponibilidade());
        Assert.assertEquals(3, catalogo.get(0).getMidias().size());
        Assert.assertEquals(MidiaType.VHS, catalogo.get(0).getMidias().get(0).getTipo());
        Assert.assertEquals(filmeDtoUpdate.getMidia().get(0).getValor(), catalogo.get(0).getMidias().get(0).getValor(), 0.0);

    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void filmeCatalogoComMidiasIndisponiveisTest() throws Exception {
        Mockito.when(localDateService.dateNow()).thenReturn(LocalDate.of(2018, 07, 20));
        Mockito.when(localDateService.now()).thenReturn(LocalDateTime.of(2018, 07, 20, 17, 0, 0));

        ArrayList<MidiaDTO> dtoMidiaList = new ArrayList<>();
        ArrayList<Long> idMidias = new ArrayList<>();
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.VHS).quantidade(1).valor(2.0).build());
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.DVD).quantidade(1).valor(3.0).build());
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.BLUE_RAY).quantidade(1).valor(4.0).build());
        FilmeDTO filmeDTO = FilmeDTO.builder()
                .titulo("Rei Arthur - remake")
                .categoria(Categoria.ACAO)
                .lancamento(LocalDate.of(2018, 11, 02))
                .midia(dtoMidiaList)
                .build();
        filmeService.salvarComMidias(filmeDTO);
        ArrayList<MidiaDTO> dtoMidiaList2 = new ArrayList<>();
        dtoMidiaList2.add(MidiaDTO.builder().tipo(MidiaType.VHS).quantidade(2).valor(2.0).build());
        dtoMidiaList2.add(MidiaDTO.builder().tipo(MidiaType.DVD).quantidade(2).valor(3.0).build());
        dtoMidiaList2.add(MidiaDTO.builder().tipo(MidiaType.BLUE_RAY).quantidade(2).valor(4.0).build());
        FilmeDTO filmeDTO2 = FilmeDTO.builder()
                .titulo("rEi lEãO")
                .categoria(Categoria.ANIMACAO)
                .lancamento(LocalDate.of(1994, 04, 06))
                .midia(dtoMidiaList2)
                .build();
        filmeService.salvarComMidias(filmeDTO2);

        Cliente cliente = Cliente.builder().nome("nome")
                .rua("rua").numero(10l).bairro("bairro").cidade("cidade").estado("estado")
                .telefone("9999999999")
                .build();
        clienteService.save(cliente);

        List<Midia> midias = midiaRepository.findAll();
        for (int i = 0; i < 6; i++) {
            idMidias.add(midias.get(i).getId());
        }

        AluguelDTO aluguelDTO = AluguelDTO.builder()
                .idCliente(cliente.getId())
                .midias(idMidias)
                .build();

        aluguelService.alugarFilme(aluguelDTO);

        Pageable pageable = Pageable.unpaged();
        FilmeCatalogoDTO dto = FilmeCatalogoDTO.builder().titulo("rei").build();
        List<CatalogoDTO> catalogo = filmeService.listarCatalogo(pageable, dto).getContent();

        Assert.assertEquals(2, catalogo.size());
        Assert.assertEquals(midias.get(0).getFilme().getId(), catalogo.get(0).getFilme().getId());
        Assert.assertEquals(midias.get(3).getFilme().getId(), catalogo.get(1).getFilme().getId());
        Assert.assertEquals("26/07/2018", catalogo.get(0).getMidias().get(0).getDisponibilidade());
        Assert.assertEquals("disponivel", catalogo.get(1).getMidias().get(2).getDisponibilidade());
        Assert.assertEquals(6, catalogo.get(0).getMidias().size() + catalogo.get(1).getMidias().size());
        Assert.assertEquals(MidiaType.VHS, catalogo.get(0).getMidias().get(0).getTipo());
        Assert.assertEquals(filmeDTO.getMidia().get(0).getValor(), catalogo.get(0).getMidias().get(0).getValor(), 0.0);

    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void filmeCatalogoFilmeNaoEncontradoTest() throws Exception {

        Pageable pageable = Pageable.unpaged();
        FilmeCatalogoDTO dto = FilmeCatalogoDTO.builder().titulo("rei").build();
        List<CatalogoDTO> catalogo = filmeService.listarCatalogo(pageable, dto).getContent();
        List listaVazia = new ArrayList();

        Assert.assertEquals(0, catalogo.size());
        Assert.assertEquals(listaVazia, catalogo);

    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void filmeSearchFilmeNaoEncontradoTest() throws Exception {

        Pageable pageable = Pageable.unpaged();
        String titulo = "rei";
        List listaVazia = new ArrayList();

        List<Filme> busca = filmeService.search(pageable, null, titulo, null, null).getContent();

        Assert.assertEquals(0, busca.size());
        Assert.assertEquals(listaVazia, busca);

    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void filmeSearchFilmesEncontradosTest() throws Exception {

        Pageable pageable = Pageable.unpaged();
        String tituloBusca = "rei";

        Filme filme1 = Filme.builder()
                .titulo("Rei Arthur - remake")
                .categoria(Categoria.ACAO)
                .lancamento(LocalDate.of(2018, 11, 02))
                .build();
        filmeService.save(filme1);

        Filme filme2 = Filme.builder()
                .titulo("rEi lEãO")
                .categoria(Categoria.ANIMACAO)
                .lancamento(LocalDate.of(1994, 04, 06))
                .build();
        filmeService.save(filme2);
        restMockMvc.perform(MockMvcRequestBuilders.get("/api/filme/search/?titulo=rei")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$").isNotEmpty());
        List<Filme> buscaComTituloRei = filmeService.search(pageable, null, tituloBusca, null, null).getContent();

        Assert.assertEquals(2, buscaComTituloRei.size());
        Assert.assertEquals(filme1.getId(), buscaComTituloRei.get(0).getId());
        Assert.assertEquals(filme1.getTitulo(), buscaComTituloRei.get(0).getTitulo());

    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void filmeCountByTipoTest() throws Exception {
        Mockito.when(localDateService.dateNow()).thenReturn(LocalDate.of(2018, 07, 20));
        Mockito.when(localDateService.now()).thenReturn(LocalDateTime.of(2018, 07, 20, 17, 0, 0));

        ArrayList<MidiaDTO> dtoMidiaList = new ArrayList<>();
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.VHS).quantidade(3).valor(2.0).build());
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.DVD).quantidade(5).valor(3.0).build());
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.BLUE_RAY).quantidade(10).valor(4.0).build());

        FilmeDTO filmeDTO = FilmeDTO.builder()
                .id(1l)
                .titulo("Rei Arthur - remake")
                .categoria(Categoria.ACAO)
                .lancamento(LocalDate.of(2018, 11, 02))
                .midia(dtoMidiaList)
                .build();

        Filme f = filmeService.salvarComMidias(filmeDTO).get();

        Long qtdeBRD = midiaRepository.countByTipo(MidiaType.BLUE_RAY);
        Long qtdeDVD = midiaRepository.countByTipoAndFilmeId(MidiaType.DVD, f.getId());

        Long contagem1 = objectMapper.readValue(
                restMockMvc.perform(MockMvcRequestBuilders.get("/api/filme/count/{id}/{tipo}", f.getId(), MidiaType.BLUE_RAY))
                        .andExpect(MockMvcResultMatchers.status().isOk())
                        .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                        .andReturn().getResponse().getContentAsString(), long.class);

        Assert.assertEquals(qtdeBRD, contagem1);

        Long contagem2 = objectMapper.readValue(
                restMockMvc.perform(MockMvcRequestBuilders.get("/api/filme/count/{id}/{tipo}", f.getId(), MidiaType.DVD))
                        .andExpect(MockMvcResultMatchers.status().isOk())
                        .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                        .andReturn().getResponse().getContentAsString(), long.class);

        Assert.assertEquals(qtdeDVD, contagem2);

    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void filmePrecosTest() throws Exception {
        Mockito.when(localDateService.dateNow()).thenReturn(LocalDate.of(2018, 07, 20));
        Mockito.when(localDateService.now()).thenReturn(LocalDateTime.of(2018, 07, 20, 17, 0, 0));

        ArrayList<MidiaDTO> dtoMidiaList = new ArrayList<>();
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.VHS).quantidade(1).valor(2.0).build());
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.DVD).quantidade(1).valor(3.0).build());
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.BLUE_RAY).quantidade(1).valor(4.0).build());
        FilmeDTO filmeDTO = FilmeDTO.builder()
                .id(1l)
                .titulo("Rei Arthur - remake")
                .categoria(Categoria.ACAO)
                .lancamento(LocalDate.of(2018, 11, 02))
                .midia(dtoMidiaList)
                .build();

        Filme f = filmeService.salvarComMidias(filmeDTO).get();

        ArrayList<MidiaDTO> dtoMidiaList2 = new ArrayList<>();
        dtoMidiaList2.add(MidiaDTO.builder().tipo(MidiaType.VHS).quantidade(1).valor(3.5).build());
        dtoMidiaList2.add(MidiaDTO.builder().tipo(MidiaType.DVD).quantidade(1).valor(5.0).build());
        dtoMidiaList2.add(MidiaDTO.builder().tipo(MidiaType.BLUE_RAY).quantidade(1).valor(7.0).build());
        FilmeDTO filmeDTO2 = FilmeDTO.builder()
                .id(f.getId())
                .titulo("Rei Arthur - remake")
                .categoria(Categoria.ACAO)
                .lancamento(LocalDate.of(2018, 11, 02))
                .midia(dtoMidiaList2)
                .build();

        f = filmeService.update(filmeDTO2).get();

        restMockMvc.perform(MockMvcRequestBuilders.get("/api/filme/precos/{id}/", f.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));

        List<PrecoDTO> precos = filmeService.precosDoFilme(f.getId());

        Assert.assertEquals(6, precos.size());

    }

}
