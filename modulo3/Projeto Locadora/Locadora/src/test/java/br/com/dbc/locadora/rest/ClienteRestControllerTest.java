/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.LocadoraApplicationTests;
import br.com.dbc.locadora.entity.Cliente;
import br.com.dbc.locadora.repository.ClienteRepository;
import br.com.dbc.locadora.service.ClienteService;
import br.com.dbc.locadora.service.CorreiosWsService;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import static org.mockito.Mockito.verify;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author tiago.schimidt
 */
public class ClienteRestControllerTest extends LocadoraApplicationTests {

    @Autowired
    private ClienteRestController clienteRestController;

    @Autowired
    private ClienteRepository clienteRepository;
    
    @Autowired
    private ClienteService clienteService;

    @Override
    protected AbstractRestController getController() {
        return clienteRestController;
    }

    @Before
    public void beforeTest() {
        clienteRepository.deleteAll();
    }

    @After
    public void tearDown() {
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void clienteCreateTest() throws Exception {
        Cliente c = Cliente.builder().nome("nome")
                .rua("rua").numero(10l).bairro("bairro").cidade("cidade").estado("estado")
                .telefone("9999999999")
                .build();
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/cliente")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(c)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(c.getNome()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.telefone").value(c.getTelefone()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.rua").value(c.getRua()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.numero").value(c.getNumero()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.bairro").value(c.getBairro()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.cidade").value(c.getCidade()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.estado").value(c.getEstado()));
        List<Cliente> clientes = clienteRepository.findAll();
        Assert.assertEquals(1, clientes.size());
        Assert.assertEquals(c.getNome(), clientes.get(0).getNome());
        Assert.assertEquals(c.getTelefone(), clientes.get(0).getTelefone());
        Assert.assertEquals(c.getRua(), clientes.get(0).getRua());
        Assert.assertEquals(c.getNumero(), clientes.get(0).getNumero());
        Assert.assertEquals(c.getBairro(), clientes.get(0).getBairro());
        Assert.assertEquals(c.getCidade(), clientes.get(0).getCidade());
        Assert.assertEquals(c.getEstado(), clientes.get(0).getEstado());
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void clienteUpdateTest() throws Exception {

        Cliente c = Cliente.builder().nome("nome")
                .rua("rua").numero(10l).bairro("bairro").cidade("cidade").estado("estado")
                .telefone("9999999999")
                .build();
        clienteRepository.save(c);
        Cliente cUpdate = Cliente.builder().id(c.getId()).nome("nome")
                .rua("rua").numero(10l).bairro("bairro").cidade("cidade").estado("estado")
                .telefone("9999999999")
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.put("/api/cliente/{id}", c.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(cUpdate)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value(cUpdate.getNome()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.telefone").value(cUpdate.getTelefone()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.rua").value(cUpdate.getRua()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.numero").value(cUpdate.getNumero()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.bairro").value(cUpdate.getBairro()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.cidade").value(cUpdate.getCidade()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.estado").value(cUpdate.getEstado()));

        List<Cliente> clientes = clienteRepository.findAll();

        Assert.assertEquals(1, clientes.size());
        Assert.assertEquals(cUpdate.getNome(), clientes.get(0).getNome());
        Assert.assertEquals(cUpdate.getTelefone(), clientes.get(0).getTelefone());
        Assert.assertEquals(cUpdate.getRua(), clientes.get(0).getRua());
        Assert.assertEquals(cUpdate.getNumero(), clientes.get(0).getNumero());
        Assert.assertEquals(cUpdate.getBairro(), clientes.get(0).getBairro());
        Assert.assertEquals(cUpdate.getCidade(), clientes.get(0).getCidade());
        Assert.assertEquals(cUpdate.getEstado(), clientes.get(0).getEstado());

    }
    
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void clienteConsultaCepTest() throws Exception {
        CorreiosWsService mock = Mockito.mock(CorreiosWsService.class);
        String cep = "90410002";        
        mock.buscarPorCep(cep);        
        restMockMvc.perform(MockMvcRequestBuilders.get("/api/cliente/cep/{cep}", cep)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));        
        verify(mock, Mockito.times(1)).buscarPorCep(cep);
        
    }
    
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void clienteFindAllTest() throws Exception {
        restMockMvc.perform(MockMvcRequestBuilders.get("/api/cliente/?page=0&?size=10")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));        
    }
    
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void clienteFindByIdTest() throws Exception {
        Cliente cliente = Cliente.builder().telefone("9999999999").nome("joão").bairro("aaa").cidade("aaa").estado("rs").numero(12l).rua("aaa").build();
        cliente = clienteService.save(cliente);
        Cliente c = objectMapper.readValue(
        restMockMvc.perform(MockMvcRequestBuilders.get("/api/cliente/{id}",cliente.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn().getResponse().getContentAsString(), Cliente.class);    
        Assert.assertEquals(cliente.getId(), c.getId());
    }
    
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void clienteDeleteTest() throws Exception {
        Cliente cliente = Cliente.builder().telefone("9999999999").nome("joão").bairro("aaa").cidade("aaa").estado("rs").numero(12l).rua("aaa").build();
        cliente = clienteService.save(cliente);
        restMockMvc.perform(MockMvcRequestBuilders.delete("/api/cliente/{id}",cliente.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

}
