/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.LocadoraApplicationTests;
import br.com.dbc.locadora.dto.FilmeDTO;
import br.com.dbc.locadora.dto.MidiaDTO;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.enumerated.Categoria;
import br.com.dbc.locadora.enumerated.MidiaType;
import br.com.dbc.locadora.repository.FilmeRepository;
import br.com.dbc.locadora.repository.MidiaRepository;
import br.com.dbc.locadora.repository.ValorMidiaRepository;
import br.com.dbc.locadora.service.FilmeService;
import br.com.dbc.locadora.service.MidiaService;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author tiago.schmidt
 */
public class MidiaRestControllerTest extends LocadoraApplicationTests {

    @Autowired
    private MidiaRestController midiaRestController;

    @Autowired
    private FilmeService filmeService;

    @Autowired
    private MidiaService midiaService;

    @Autowired
    private ValorMidiaRepository valorMidiaRepository;

    @Autowired
    private MidiaRepository midiaRepository;

    @Autowired
    private FilmeRepository filmeRepository;

    @Override
    protected AbstractRestController getController() {
        return midiaRestController;
    }

    @Before
    public void beforeTest() {
        super.setUp();
        Mockito.reset();
        valorMidiaRepository.deleteAll();
        midiaRepository.deleteAll();
        filmeRepository.deleteAll();
    }

    @After
    public void tearDown() {
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void midiaCountByTipoTest() throws Exception {
        ArrayList<MidiaDTO> dtoMidiaList = new ArrayList<>();
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.DVD).quantidade(5).valor(3.0).build());
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.VHS).quantidade(5).valor(3.0).build());
        FilmeDTO filmeDTO = FilmeDTO.builder()
                .titulo("rei arthur")
                .categoria(Categoria.ACAO)
                .lancamento(LocalDate.of(2018, 11, 02))
                .midia(dtoMidiaList)
                .build();
        Filme f = filmeService.salvarComMidias(filmeDTO).get();
        Long contagemDVD = objectMapper.readValue(
        restMockMvc.perform(MockMvcRequestBuilders.get("/api/midia/count/{tipo}",MidiaType.DVD)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn().getResponse().getContentAsString(), Long.class);
        Long contagemVHS = midiaService.countByTipoAndFilmeId(MidiaType.VHS,f.getId());
        Assert.assertEquals(contagemDVD, midiaService.findByFilmeIdAndTipoAndAluguelIsNull(f.getId(),MidiaType.DVD).size(), 0.0);
        Assert.assertEquals(5, contagemVHS, 0.0);
    }
    
    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void midiaSearchTest() throws Exception {
        ArrayList<MidiaDTO> dtoMidiaList = new ArrayList<>();
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.DVD).quantidade(5).valor(3.0).build());
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.VHS).quantidade(5).valor(3.0).build());
        FilmeDTO filmeDTO = FilmeDTO.builder()
                .titulo("rei arthur")
                .categoria(Categoria.ACAO)
                .lancamento(LocalDate.of(2018, 11, 02))
                .midia(dtoMidiaList)
                .build();
        Filme f = filmeService.salvarComMidias(filmeDTO).get();
        restMockMvc.perform(MockMvcRequestBuilders.get("/api/midia/search?titulo=rei")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
        Pageable pageable = Pageable.unpaged();
        List<Midia> midias = midiaService.search(pageable, f.getTitulo(), null, null, null).getContent();
        
        Assert.assertEquals(10, midias.size(), 0.0);
    }

}
