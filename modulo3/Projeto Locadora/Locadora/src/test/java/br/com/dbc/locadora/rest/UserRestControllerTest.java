/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.LocadoraApplicationTests;
import br.com.dbc.locadora.entity.User;
import br.com.dbc.locadora.repository.UserRepository;
import br.com.dbc.locadora.service.UserService;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author tiago.schmidt
 */
public class UserRestControllerTest extends LocadoraApplicationTests {
    
    @Autowired
    private UserRestController userRestController;
    
    @Override
    protected AbstractRestController getController() {
        return userRestController;
    }
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private UserService userService;
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void beforeTest() {
        userRepository.deleteAll();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void testCreate() throws Exception {
        
        User user = User.builder()
                .firstName("Usuario")
                .lastName("1")
                .username("user")
                .password("password")
                .build();
        
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/user")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(user)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(user.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(user.getLastName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value(user.getUsername()));
        
        List<User> usuarios = userRepository.findAll();
        User usuario = userRepository.findByUsername(user.getUsername());
        
        Assert.assertEquals(1, usuarios.size());
        Assert.assertEquals("STANDARD_USER", usuarios.get(0).getRoles().get(0).getRoleName());
        Assert.assertEquals("Usuario1", usuarios.get(0).getFirstName() + usuarios.get(0).getLastName());
        Assert.assertEquals("user", usuarios.get(0).getUsername());
        Assert.assertEquals(usuario.getPassword(), usuarios.get(0).getPassword());  
        
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void testChangePassword() throws Exception {
        
        User user = User.builder()
                .firstName("Usuario")
                .lastName("1")
                .username("user")
                .password("password")
                .build();
        
        user = userService.create(user);
        
        user.setPassword("new_password");
        
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/user/password")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(user)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(user.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(user.getLastName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value(user.getUsername()));
        
        List<User> usuarios = userRepository.findAll();
        User usuario = userRepository.findByUsername(user.getUsername());
        
        Assert.assertEquals(1, usuarios.size());
        Assert.assertEquals("STANDARD_USER", usuarios.get(0).getRoles().get(0).getRoleName());
        Assert.assertEquals("Usuario1", usuarios.get(0).getFirstName() + usuarios.get(0).getLastName());
        Assert.assertEquals("user", usuarios.get(0).getUsername());
        Assert.assertEquals(usuario.getPassword(), usuarios.get(0).getPassword());
        
    }
    
    public void testChangePasswordWithSamePassword() throws Exception {
        
        User user = User.builder()
                .firstName("Usuario")
                .lastName("1")
                .username("user")
                .password("password")
                .build();
        
        user = userService.create(user);        
        user.setPassword("password");
        
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/user/password")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(user)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(user.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value(user.getLastName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value(user.getUsername()));
        
        List<User> usuarios = userRepository.findAll();
        User usuario = userRepository.findByUsername(user.getUsername());
        
        Assert.assertEquals(1, usuarios.size());
        Assert.assertEquals(usuario.getPassword(), usuarios.get(0).getPassword());
        
    }
    
    @Test(expected = Exception.class)
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void testExceptionsUser() throws Exception {
        
        User user = User.builder()
                .firstName("Usuario")
                .lastName("1")
                .username("user")
                .password("password")
                .build();
        
        user = userService.create(user);
        
        User user2 = User.builder()
                .firstName("seila")
                .lastName("seila")
                .username("seila")
                .password("seila")
                .build();
        
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/user/password")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(user2)))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
        
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/user")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(user)))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
        
    }
    
}
