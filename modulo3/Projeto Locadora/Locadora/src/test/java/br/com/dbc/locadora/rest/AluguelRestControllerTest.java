/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.LocadoraApplicationTests;
import br.com.dbc.locadora.dto.AluguelDTO;
import br.com.dbc.locadora.dto.DevolucaoAluguelDTO;
import br.com.dbc.locadora.dto.FilmeDTO;
import br.com.dbc.locadora.dto.MidiaDTO;
import br.com.dbc.locadora.dto.RetornoAluguelDTO;
import br.com.dbc.locadora.entity.Aluguel;
import br.com.dbc.locadora.entity.Cliente;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.enumerated.Categoria;
import br.com.dbc.locadora.enumerated.MidiaType;
import br.com.dbc.locadora.repository.AluguelRepository;
import br.com.dbc.locadora.repository.ClienteRepository;
import br.com.dbc.locadora.repository.FilmeRepository;
import br.com.dbc.locadora.repository.MidiaRepository;
import br.com.dbc.locadora.repository.ValorMidiaRepository;
import br.com.dbc.locadora.service.AluguelService;
import br.com.dbc.locadora.service.ClienteService;
import br.com.dbc.locadora.service.FilmeService;
import br.com.dbc.locadora.service.LocalDateService;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author tiago.schmidt
 */
public class AluguelRestControllerTest extends LocadoraApplicationTests {

    @Autowired
    private AluguelRestController aluguelRestController;

    @MockBean
    private LocalDateService localDateService;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private AluguelService aluguelService;

    @Autowired
    private AluguelRepository aluguelRepository;

    @Autowired
    private MidiaRepository midiaRepository;

    @Autowired
    private ValorMidiaRepository valorMidiaRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private FilmeRepository filmeRepository;

    @Autowired
    private FilmeService filmeService;

    @Override
    protected AbstractRestController getController() {
        return aluguelRestController;
    }

    @Before
    public void beforeTest() {
        super.setUp();
        Mockito.reset();
        valorMidiaRepository.deleteAll();
        midiaRepository.deleteAll();
        filmeRepository.deleteAll();
        aluguelRepository.deleteAll();
    }

    @After
    public void tearDown() {
    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void aluguelRetiradaTest() throws Exception {
        Mockito.when(localDateService.dateNow()).thenReturn(LocalDate.of(2018, 07, 20));
        Mockito.when(localDateService.now()).thenReturn(LocalDateTime.of(2018, 07, 20, 17, 0, 0));

        ArrayList<MidiaDTO> dtoMidiaList = new ArrayList<>();
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.VHS).quantidade(1).valor(2.0).build());
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.DVD).quantidade(1).valor(3.0).build());
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.BLUE_RAY).quantidade(1).valor(4.0).build());

        FilmeDTO filmeDTO = FilmeDTO.builder()
                .titulo("rei arthur")
                .categoria(Categoria.ACAO)
                .lancamento(LocalDate.of(2018, 11, 02))
                .midia(dtoMidiaList)
                .build();

        filmeService.salvarComMidias(filmeDTO);

        Cliente cliente = Cliente.builder().nome("nome")
                .rua("rua").numero(10l).bairro("bairro").cidade("cidade").estado("estado")
                .telefone("9999999999")
                .build();
        clienteService.save(cliente);

        ArrayList<Long> idMidias = new ArrayList<>();
        List<Midia> midias = midiaRepository.findAll();

        for (int i = 0; i < midias.size(); i++) {
            idMidias.add(midias.get(i).getId());
        }

        AluguelDTO aluguelDTO = AluguelDTO.builder()
                .idCliente(cliente.getId())
                .midias(idMidias)
                .build();

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/aluguel/retirada")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(aluguelDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.valorAluguel").value(9.0))
                .andExpect(MockMvcResultMatchers.jsonPath("$.retirada").value(LocalDate.of(2018, 07, 20).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.previsao").value(LocalDate.of(2018, 07, 23).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))));

        List<Aluguel> alugueis = aluguelRepository.findAll();
        List<Cliente> clientesSalvos = clienteRepository.findAll();
        List<Midia> midiasSalvas = midiaRepository.findAll();
        List<ValorMidia> valoresSalvos = valorMidiaRepository.findAll();

        Double somaValoresMidiasSalvas = 0.0;

        for (int i = 0; i < valoresSalvos.size(); i++) {
            somaValoresMidiasSalvas += valoresSalvos.get(i).getValor();
        }

        Assert.assertEquals(1, alugueis.size());
        Assert.assertEquals(clientesSalvos.get(0), alugueis.get(0).getCliente());
        Assert.assertEquals(LocalDate.of(2018, 07, 23), alugueis.get(0).getPrevisao());
        Assert.assertEquals(LocalDate.of(2018, 07, 20), alugueis.get(0).getRetirada());
        Assert.assertEquals(null, alugueis.get(0).getDevolucao());
        Assert.assertEquals(null, alugueis.get(0).getMulta());
        Assert.assertEquals(midiasSalvas.get(0).getAluguel().getId(), alugueis.get(0).getId());
        Assert.assertEquals(9.0, somaValoresMidiasSalvas, 0.0);

    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void aluguelDevolucaoTest() throws Exception {
        Mockito.when(localDateService.dateNow()).thenReturn(LocalDate.of(2018, 07, 20));
        Mockito.when(localDateService.now()).thenReturn(LocalDateTime.of(2018, 07, 20, 17, 0, 0));

        ArrayList<MidiaDTO> dtoMidiaList = new ArrayList<>();
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.VHS).quantidade(1).valor(2.0).build());
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.DVD).quantidade(1).valor(3.0).build());
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.BLUE_RAY).quantidade(1).valor(4.0).build());

        FilmeDTO filmeDTO = FilmeDTO.builder()
                .titulo("rei arthur")
                .categoria(Categoria.ACAO)
                .lancamento(LocalDate.of(2018, 11, 02))
                .midia(dtoMidiaList)
                .build();

        filmeService.salvarComMidias(filmeDTO);

        Cliente cliente = Cliente.builder().nome("nome")
                .rua("rua").numero(10l).bairro("bairro").cidade("cidade").estado("estado")
                .telefone("9999999999")
                .build();
        clienteService.save(cliente);

        ArrayList<Long> idMidias = new ArrayList<>();
        List<Midia> midias = midiaRepository.findAll();

        for (int i = 0; i < midias.size(); i++) {
            idMidias.add(midias.get(i).getId());
        }

        AluguelDTO aluguelDTO = AluguelDTO.builder()
                .idCliente(cliente.getId())
                .midias(idMidias)
                .build();

        DevolucaoAluguelDTO devolucaoDTO = DevolucaoAluguelDTO.builder().midias(idMidias).build();

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/aluguel/retirada")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(aluguelDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.valorAluguel").value(9.0))
                .andExpect(MockMvcResultMatchers.jsonPath("$.retirada").value(LocalDate.of(2018, 07, 20).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.previsao").value(LocalDate.of(2018, 07, 23).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))));

        List<Aluguel> alugueis = aluguelRepository.findAll();

        System.out.println(devolucaoDTO);
        aluguelService.devolverFilme(devolucaoDTO);

        List<Cliente> clientesSalvos = clienteRepository.findAll();
        List<Midia> midiasSalvas = midiaRepository.findAll();
        List<ValorMidia> valoresSalvos = valorMidiaRepository.findAll();
        List<Aluguel> alugueisAtualizados = aluguelRepository.findAll();

        Double somaValoresMidiasSalvas = 0.0;
        for (int i = 0; i < valoresSalvos.size(); i++) {
            somaValoresMidiasSalvas += valoresSalvos.get(i).getValor();
        }

        Assert.assertEquals(1, alugueis.size());
        Assert.assertEquals(clientesSalvos.get(0), alugueis.get(0).getCliente());
        Assert.assertEquals(LocalDate.of(2018, 07, 23), alugueisAtualizados.get(0).getPrevisao());
        Assert.assertEquals(LocalDate.of(2018, 07, 20), alugueisAtualizados.get(0).getRetirada());
        Assert.assertEquals(null, alugueis.get(0).getDevolucao());
        Assert.assertEquals(LocalDateTime.of(2018, 07, 20, 17, 0, 0), alugueisAtualizados.get(0).getDevolucao());
        Assert.assertEquals(null, alugueis.get(0).getMulta());
        Assert.assertEquals(null, alugueisAtualizados.get(0).getMulta());
        Assert.assertEquals(null, midiasSalvas.get(0).getAluguel());
        Assert.assertEquals(9.0, somaValoresMidiasSalvas, 0.0);

    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void aluguelDevolucaoComMultaTest() throws Exception {
        Mockito.when(localDateService.dateNow()).thenReturn(LocalDate.of(2018, 07, 20));
        Mockito.when(localDateService.now()).thenReturn(LocalDateTime.of(2018, 07, 24, 17, 0, 0));

        ArrayList<MidiaDTO> dtoMidiaList = new ArrayList<>();
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.VHS).quantidade(1).valor(2.0).build());
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.DVD).quantidade(1).valor(3.0).build());
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.BLUE_RAY).quantidade(1).valor(4.0).build());

        FilmeDTO filmeDTO = FilmeDTO.builder()
                .titulo("rei arthur")
                .categoria(Categoria.ACAO)
                .lancamento(LocalDate.of(2018, 11, 02))
                .midia(dtoMidiaList)
                .build();

        filmeService.salvarComMidias(filmeDTO);

        Cliente cliente = Cliente.builder().nome("nome")
                .rua("rua").numero(10l).bairro("bairro").cidade("cidade").estado("estado")
                .telefone("9999999999")
                .build();
        clienteService.save(cliente);

        ArrayList<Long> idMidias = new ArrayList<>();
        List<Midia> midias = midiaRepository.findAll();

        for (int i = 0; i < midias.size(); i++) {
            idMidias.add(midias.get(i).getId());
        }

        AluguelDTO aluguelDTO = AluguelDTO.builder()
                .idCliente(cliente.getId())
                .midias(idMidias)
                .build();

        aluguelService.alugarFilme(aluguelDTO);

        restMockMvc.perform(MockMvcRequestBuilders.post("/api/aluguel/devolucao")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(aluguelDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.valorAluguel").value(18.0))
                .andExpect(MockMvcResultMatchers.jsonPath("$.retirada").value(LocalDate.of(2018, 07, 20).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.previsao").value(LocalDate.of(2018, 07, 23).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.devolucao").value(LocalDateTime.of(2018, 07, 24, 17, 0, 0).format(DateTimeFormatter.ofPattern("dd/MM/yyyy'T'HH:mm:ss"))));

        List<ValorMidia> valoresSalvos = valorMidiaRepository.findAll();
        List<Aluguel> alugueis = aluguelRepository.findAll();

        Double somaValoresMidiasSalvas = 0.0;

        for (int i = 0; i < valoresSalvos.size(); i++) {
            somaValoresMidiasSalvas += valoresSalvos.get(i).getValor();
        }

        Assert.assertEquals(LocalDate.of(2018, 07, 23), alugueis.get(0).getPrevisao());
        Assert.assertEquals(LocalDate.of(2018, 07, 20), alugueis.get(0).getRetirada());
        Assert.assertEquals(LocalDateTime.of(2018, 07, 24, 17, 0, 0), alugueis.get(0).getDevolucao());
        Assert.assertEquals(9.0, alugueis.get(0).getMulta(), 0.0);
        Assert.assertEquals(9.0, somaValoresMidiasSalvas, 0.0);

    }

    @Test
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void aluguelDevolucoesHojeTest() throws Exception {
        Mockito.when(localDateService.dateNow()).thenReturn(LocalDate.now().minusDays(3));
        Mockito.when(localDateService.now()).thenReturn(LocalDateTime.now());
        ArrayList<MidiaDTO> dtoMidiaList = new ArrayList<>();
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.VHS).quantidade(1).valor(2.0).build());
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.DVD).quantidade(1).valor(3.0).build());
        dtoMidiaList.add(MidiaDTO.builder().tipo(MidiaType.BLUE_RAY).quantidade(1).valor(4.0).build());
        FilmeDTO filmeDTO = FilmeDTO.builder()
                .titulo("rei arthur")
                .categoria(Categoria.ACAO)
                .lancamento(LocalDate.of(2018, 11, 02))
                .midia(dtoMidiaList)
                .build();
        filmeService.salvarComMidias(filmeDTO);
        Cliente cliente = Cliente.builder().nome("nome")
                .rua("rua").numero(10l).bairro("bairro").cidade("cidade").estado("estado")
                .telefone("9999999999")
                .build();
        clienteService.save(cliente);
        ArrayList<Long> idMidias = new ArrayList<>();
        List<Midia> midias = midiaRepository.findAll();
        for (int i = 0; i < midias.size(); i++) {
            idMidias.add(midias.get(i).getId());
        }
        AluguelDTO aluguelDTO = AluguelDTO.builder()
                .idCliente(cliente.getId())
                .midias(idMidias)
                .build();
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/aluguel/retirada")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(aluguelDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.valorAluguel").value(9.0))
                .andExpect(MockMvcResultMatchers.jsonPath("$.retirada").value(LocalDate.now().minusDays(midias.size()).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))))
                .andExpect(MockMvcResultMatchers.jsonPath("$.previsao").value(LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))));

        Pageable pageable = Pageable.unpaged();
        List<Filme> devolucoesHoje = aluguelService.listarDevolucoesHoje(pageable).getContent();

        Assert.assertEquals(1, devolucoesHoje.size());

    }
    
    @Test(expected = Exception.class)
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    public void aluguelExceptionsTest() throws Exception {
        Mockito.when(localDateService.dateNow()).thenReturn(LocalDate.now());
        Mockito.when(localDateService.now()).thenReturn(LocalDateTime.now());
        FilmeDTO filmeDTO = FilmeDTO.builder().build();
        filmeService.salvarComMidias(filmeDTO);
        Cliente cliente = Cliente.builder().build();
        clienteService.save(cliente);
        ArrayList<Long> idMidias = new ArrayList<>();
        AluguelDTO aluguelDTO = AluguelDTO.builder().idCliente(cliente.getId()).midias(idMidias).build();
        restMockMvc.perform(MockMvcRequestBuilders.get("/api/aluguel/devolucao"));
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/aluguel/retirada")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(aluguelDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
        restMockMvc.perform(MockMvcRequestBuilders.post("/api/aluguel/devolucao")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(aluguelDTO)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

}
