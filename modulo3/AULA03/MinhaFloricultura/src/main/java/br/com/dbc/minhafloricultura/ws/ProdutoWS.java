/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.minhafloricultura.ws;

import br.com.dbc.minhafloricultura.dao.ProdutoDAO;
import br.com.dbc.minhafloricultura.entity.Produto;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tiago.schmidt
 */
@Stateless
@WebService(serviceName = "ProdutoWS")
public class ProdutoWS extends AbstractCrudWS<ProdutoDAO, Produto> {

    @EJB
    private ProdutoDAO produtoDAO;

    @Override
    public ProdutoDAO getDAO() {
        return produtoDAO;
    }    
    
    @WebMethod(operationName = "findByDescriptionSOAP")
    public List<Produto> findByDescriptionSOAP(@WebParam(name = "descricao") String descricao) {
        return produtoDAO.findByDescriptionSOAP(descricao);
    }
    
}
