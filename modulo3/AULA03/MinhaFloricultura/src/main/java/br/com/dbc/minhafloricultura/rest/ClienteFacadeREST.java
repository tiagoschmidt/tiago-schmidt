/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.minhafloricultura.rest;

import br.com.dbc.minhafloricultura.dao.ClienteDAO;
import br.com.dbc.minhafloricultura.entity.Cliente;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

/**
 *
 * @author tiago.schmidt
 */
@Stateless
@Path("cliente")
public class ClienteFacadeREST extends AbstractCrudREST<Cliente, ClienteDAO> {

    @Inject
    private ClienteDAO clienteDAO;

    @Override
    protected ClienteDAO getDAO() {
        return clienteDAO;
    }
    
}
