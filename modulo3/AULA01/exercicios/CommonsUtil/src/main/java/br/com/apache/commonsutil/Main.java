/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.apache.commonsutil;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Tiago
 */
public class Main {
    
    public static void main(String[] args) {
        
        System.out.println(StringUtils.isBlank(" "));
        System.out.println(StringUtils.contains("texto qualquer"," "));
        System.out.println(StringUtils.leftPad("teste", 8) + " esquerda");
        System.out.println(StringUtils.rightPad("teste", 8) + "direita");
        System.out.println(StringUtils.equals("        testeEquals ", "        testeEquals "));
        System.out.println(StringUtils.trimToNull("               teste             "));  
    }
}
