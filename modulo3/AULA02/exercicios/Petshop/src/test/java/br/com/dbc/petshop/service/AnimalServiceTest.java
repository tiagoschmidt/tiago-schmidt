/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshop.service;

import br.com.dbc.petshop.entity.Animal;
import br.com.dbc.petshop.entity.Cliente;
import br.com.dbc.petshop.entity.HibernateUtil;
import br.com.dbc.petshop.entity.PersistenceUtils;
import br.com.dbc.petshop.entity.SexoType;
import java.util.List;
import org.hibernate.Session;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author tiago.schmidt
 */
public class AnimalServiceTest {
    
    public AnimalServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        PersistenceUtils.getEm();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        PersistenceUtils.beginTransaction();
        PersistenceUtils.getEm().createNativeQuery("delete from animal").executeUpdate();
        PersistenceUtils.getEm().createNativeQuery("delete from cliente").executeUpdate();
        PersistenceUtils.getEm().getTransaction().commit();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of inserirCincoAnimaisComHibernate method, of class AnimalService.
     */
    @Test
    public void testInserirCincoAnimaisComHibernate() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.getTransaction().begin();
        System.out.println("inserirCincoAnimaisComHibernate");
        Cliente cliente = new Cliente(null, "cliente", SexoType.M, "Profissional");
        session.save(cliente);
        session.getTransaction().commit();
        AnimalService instance = AnimalService.getInstance();
        instance.inserirCincoAnimaisComHibernate(cliente);
        List<Animal> resultado = session.createCriteria(Animal.class).list();
        assertEquals(5,resultado.size());        
    }    
}
