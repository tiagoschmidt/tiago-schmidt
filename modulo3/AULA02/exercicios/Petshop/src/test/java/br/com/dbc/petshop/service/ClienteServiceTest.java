/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshop.service;

import br.com.dbc.petshop.entity.Cliente;
import br.com.dbc.petshop.entity.PersistenceUtils;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author tiago
 */
public class ClienteServiceTest {
    
    public ClienteServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        PersistenceUtils.beginTransaction();
        PersistenceUtils.getEm().createNativeQuery("delete from animal").executeUpdate();
        PersistenceUtils.getEm().createNativeQuery("delete from cliente").executeUpdate();
        PersistenceUtils.getEm().getTransaction().commit();        
    }
    
    @After
    public void tearDown() {
    }
    
    private void createClienteNome(){
        
    }

    /**
     * Test of createCriteria method, of class ClienteService.
     */
    @Test
    public void testInserirDezClientesComDezAnimais() {
        EntityManager em = PersistenceUtils.getEm();
        System.out.println("testInserirDezClientesComDezAnimais");
        ClienteService instance = ClienteService.getInstance();
        instance.inserirDezClientesComDezAnimais();
        List<Cliente> resultado = em.createQuery("select c from Cliente c", Cliente.class).getResultList();
        assertEquals(10,resultado.size());
    }  
    
}
