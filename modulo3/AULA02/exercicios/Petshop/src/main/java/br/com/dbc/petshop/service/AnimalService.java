/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshop.service;

import br.com.dbc.petshop.entity.Animal;
import br.com.dbc.petshop.entity.Cliente;
import br.com.dbc.petshop.entity.HibernateUtil;
import br.com.dbc.petshop.entity.PersistenceUtils;
import br.com.dbc.petshop.entity.SexoType;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author tiago
 */
public class AnimalService {

    private static final AnimalService instance;

    static {
        instance = new AnimalService();
    }

    public static AnimalService getInstance() {
        return instance;
    }

    private AnimalService() {
    }
    
    public void inserirDezAnimais(Cliente cliente){        
        EntityManager em = PersistenceUtils.getEm();
        PersistenceUtils.beginTransaction();
        for(int j=0; j<10; j++){
            SexoType sexo;
            BigDecimal v = BigDecimal.valueOf (j);
            if(j % 2 == 0)
                sexo = SexoType.M;
            else
                sexo = SexoType.F;
            BigDecimal valor = v;
            Animal animal = new Animal(null, "animal"+(j+1), sexo, cliente, valor);
            em.persist(animal);
        }
        em.getTransaction().commit();
    }
    
    public void inserirCincoAnimaisComHibernate(Cliente cliente){
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.getTransaction().begin();
        for(int j=0; j<5; j++){
            BigDecimal v = BigDecimal.valueOf (j);
            BigDecimal valor = v;
            SexoType sexo;
            if(j % 2 == 0)
                sexo = SexoType.M;
            else
                sexo = SexoType.F;            
            Animal animal = new Animal(null, "animal"+(j+1), sexo, cliente, valor);
            session.save(animal);
        }
            session.getTransaction().commit();
    }
    
    public List<Animal> findAll(){
        EntityManager em = PersistenceUtils.getEm();
        List<Animal> animais = em.createQuery("select a from Animal a").getResultList();
        return animais;
    }
    
    public List<Animal> findAllCriteria(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        return session.createCriteria(Animal.class).list();
    }
    
}
