/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshophibernate.service;

import br.com.dbc.petshopjpa.dao.AbstractDAO;
import br.com.dbc.petshopjpa.entity.AbstractEntity;
import java.util.List;
import javassist.NotFoundException;

/**
 *
 * @author tiago.schmidt
 * @param <E>
 * @param <I>
 * @param <DAO>
 */
public abstract class AbstractCrudService<E extends AbstractEntity<I>, I, DAO extends AbstractDAO<E, I>>{
    
    protected abstract DAO getDAO();
    
    public List<E> findAll(){
        return getDAO().findAll();
    }
    
    public E findOne(I id){
        return getDAO().findOne(id);
    }
    
    public void create(E entity){
        if(entity.getId() != null)
            throw new IllegalArgumentException("Criação de cliente não pode ter ID");
        getDAO().create(entity);
    }
    
    public void update(E entity){
        if(entity.getId() == null)
            throw new IllegalArgumentException("Atualização de cliente deve ter ID");
        getDAO().update(entity);
    }
    
    public void delete(I id) throws NotFoundException{
        if(id == null)
            throw new IllegalArgumentException("ID deve ser informado para remoção");
        E entity = findOne(id);
        if(entity == null)
            throw new NotFoundException("Cliente não encontrado");
        getDAO().delete(id);
    }    
}
