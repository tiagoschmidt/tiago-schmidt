/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshophibernate.service;

import br.com.dbc.petshopjpa.dao.ClienteDAO;
import br.com.dbc.petshopjpa.dao.PersistenceUtils;
import br.com.dbc.petshopjpa.entity.Animal;
import br.com.dbc.petshopjpa.entity.Cliente;
import java.util.Arrays;
import java.util.List;
import javassist.NotFoundException;
import javax.persistence.EntityManager;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.mockito.internal.util.reflection.Whitebox;

/**
 *
 * @author tiago.schmidt
 */
public class ClienteServiceTest {
        
    private final EntityManager em = PersistenceUtils.getEntityManager();
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        PersistenceUtils.beginTransaction();
        em.createQuery("delete from Cliente").executeUpdate();
        em.getTransaction().commit();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testFindAll(){
        System.out.println("Testar metodo findAll cliente");
        ClienteService clienteService = new ClienteService();
        Cliente cliente = Cliente.builder()
                .nome("cliente1")
                .animalList(Arrays.asList(Animal.builder()
                            .nome("animal1")
                            .build()))
                .build();
        em.persist(cliente);
        PersistenceUtils.beginTransaction();
        em.getTransaction().commit();
        List<Cliente> result = clienteService.findAll();
        
        assertEquals(1,result.size());
        assertEquals(cliente.getId(),result.stream().findFirst().get().getId());
        assertEquals(cliente.getAnimalList().get(0).getId(),
                result.stream().findFirst().get().getAnimalList().get(0).getId());
    }
    
    @Test
    public void testFindOne(){
        System.out.println("Testar metodo findOne cliente");
        ClienteService clienteService = new ClienteService();
        Cliente cliente = Cliente.builder()
                .nome("cliente1")
                .animalList(Arrays.asList(Animal.builder()
                            .nome("animal1")
                            .build()))
                .build();
        em.persist(cliente);
        PersistenceUtils.beginTransaction();
        em.getTransaction().commit();
        
        Cliente result = clienteService.findOne(cliente.getId());
        
        assertEquals(cliente.getId(),result.getId());
        assertEquals(cliente.getNome(),result.getNome());
        assertEquals(cliente.getAnimalList().get(0).getNome(),
                result.getAnimalList().get(0).getNome());
    }
    
    @Test
    public void testFindOneMocked(){
        System.out.println("Testar findOne com mock");
        ClienteService clienteService = new ClienteService();
        ClienteDAO clienteDAO = Mockito.mock(ClienteDAO.class);
        Whitebox.setInternalState(clienteService, "clienteDAO", clienteDAO);
        Cliente cliente = Cliente.builder()
                        .id(1l)
                        .nome("cliente1")
                        .animalList(Arrays.asList(Animal.builder()
                                    .nome("animal")
                                    .build()))
                        .build();        
        Mockito.doReturn(cliente).when(clienteDAO).findOne(cliente.getId());
        Cliente result = clienteService.findOne(cliente.getId());
        assertEquals(cliente.getId(),result.getId());
        verify(clienteDAO, times(1)).findOne(cliente.getId());
    }
    
    @Test
    public void testDeleteMocked() throws NotFoundException{
        System.out.println("Testar delete com mock");
        ClienteService clienteService = new ClienteService();
        ClienteDAO clienteDAO = Mockito.mock(ClienteDAO.class);
        Whitebox.setInternalState(clienteService, "clienteDAO", clienteDAO);
        Cliente cliente = Cliente.builder()
                        .id(1l)
                        .nome("cliente1")
                        .animalList(Arrays.asList(Animal.builder()
                                    .nome("animal")
                                    .build()))
                        .build();        
        Mockito.doNothing().when(clienteDAO).delete(cliente.getId());
        Mockito.doReturn(cliente).when(clienteDAO).findOne(cliente.getId());
        clienteService.delete(cliente.getId());
        verify(clienteDAO, times(1)).delete(cliente.getId());
    }
}
