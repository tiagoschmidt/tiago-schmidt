/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshophibernate.entity;

/**
 *
 * @author tiago.schmidt
 */
public abstract class AbstractEntity<ID> {
    
    public abstract ID getId();
    
}
