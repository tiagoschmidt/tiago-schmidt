/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshophibernate.service;

import br.com.dbc.petshophibernate.dao.ClienteDAO;
import br.com.dbc.petshophibernate.dao.HibernateUtil;
import br.com.dbc.petshophibernate.entity.Animal;
import br.com.dbc.petshophibernate.entity.Cliente;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javassist.NotFoundException;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 *
 * @author tiago.schmidt
 */
public class ClienteServiceTest {
    
    public ClienteServiceTest() {}
    
    @BeforeClass
    public static void setUpClass() {}
    
    @AfterClass
    public static void tearDownClass() {}
    
    @Before
    public void setUp() {}
    
    @After
    public void tearDown() {}
    
    @Test
    public void testCreate() {
        System.out.println("Criar Cliente");
        
        Cliente cliente = Cliente.builder()
                .nome("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                            .nome("Animal 1")
                            .build()))
                .build();
        
        ClienteService.getInstance().create(cliente);
        Session session = HibernateUtil.getSessionFactory().openSession();
        
        List<Cliente> clientes = session.createCriteria(Cliente.class).list();
        assertEquals("Quantidade de clientes errada", 1, clientes.size());
        
        Cliente result = clientes.stream().findAny().get();
        assertEquals("Cliente diferente", cliente.getId(), result.getId());
        
        assertEquals("Quantidade de animais diferente", 
                cliente.getAnimalList().size(),
                result.getAnimalList().size());
        
        assertEquals("Animal diferente", 
                cliente.getAnimalList().stream().findAny().get().getId(),
                result.getAnimalList().stream().findAny().get().getId());
        session.close();
    }
    
    @Test
    public void testFindOneMocked() {
        System.out.println("Testar busca de Cliente com Mock");
        Cliente cliente = Cliente.builder()
                .id(1l)
                .nome("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                            .nome("Animal 1")
                            .build()))
                .build();
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);
        Mockito.when(clienteService.findOne(cliente.getId())).thenReturn(cliente);
        clienteService.findOne(cliente.getId());   
        verify(daoMock, times(1)).findById(cliente.getId());
    }
    
    @Test
    public void testFindAllMocked() {
        System.out.println("Testar buscar todos Clientes com Mock");
        Cliente cliente = Cliente.builder()
                .id(1l)
                .nome("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                            .nome("Animal 1")
                            .build()))
                .build();
        List<Cliente> clientes = new ArrayList<>();
        clientes.add(cliente);
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);
        Mockito.when(clienteService.findAll()).thenReturn(clientes);
        clienteService.findAll();   
        verify(daoMock, times(1)).findAll();
    }
    
    @Test
    public void testCreateMocked() {
        System.out.println("Criar Cliente com Mock");
        Cliente cliente = Cliente.builder()
                .nome("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                            .nome("Animal 1")
                            .build()))
                .build();
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doNothing().when(daoMock).createOrUpdate(cliente);
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);
        clienteService.create(cliente);
        
        verify(daoMock, times(1)).createOrUpdate(cliente);
    }
    
    @Test(expected = HibernateException.class)
    public void testCreateHibernateExceptionMocked() {
        System.out.println("Criar Cliente com Mock pegando hibernate exception");
        Cliente cliente = Cliente.builder()
                .nome("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                            .nome("Animal 1")
                            .build()))
                .build();
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doThrow(HibernateException.class).when(daoMock).createOrUpdate(cliente);
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);
        clienteService.create(cliente);
    }
    
    @Test
    public void testUpdateMocked() {
        System.out.println("Atualizar Cliente com Mock");
        Cliente cliente = Cliente.builder()
                .id(1l)
                .nome("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                            .nome("Animal 1")
                            .build()))
                .build();
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doNothing().when(daoMock).createOrUpdate(cliente);
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);
        clienteService.update(cliente);   
        verify(daoMock, times(1)).createOrUpdate(cliente);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testCreateException() {
        System.out.println("Testar exception do metodo create");
        ClienteService.getInstance()
                .create(Cliente.builder()
                            .id(1l).build());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testUpdateException() {
        System.out.println("Testar exception do metodo update");
        ClienteService.getInstance()
                .update(Cliente.builder()
                            .id(null).build());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testDeleteNoIdException() throws NotFoundException {
        System.out.println("Testar exception do metodo delete com id sendo nulo");
        ClienteService.getInstance()
                .delete(null);
    }
    
    @Test(expected = NotFoundException.class)
    public void testDeleteClientNotFoundException() throws NotFoundException {
        System.out.println("Testar exception do metodo delete sem encontrar o cliente");
        ClienteService.getInstance()
                .delete(2l);
    }    
    
    @Test
    public void testDeleteMocked() throws NotFoundException {
        System.out.println("Deletar Cliente com Mock");
        Cliente cliente = Cliente.builder()
                .id(1l)
                .nome("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                            .nome("Animal 1")
                            .build()))
                .build();
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doNothing().when(daoMock).createOrUpdate(cliente);
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDAO()).thenReturn(daoMock);
        Mockito.when(clienteService.findOne(cliente.getId())).thenReturn(cliente);
        clienteService.delete(cliente.getId());
        
        verify(daoMock, times(1)).delete(cliente);
    }
}
