create table pais(id number not null primary key, nome varchar2(100) not null);
drop table estado;
create table estado(id number not null primary key, nome varchar2(100) not null, id_pais number not null references pais(id) );
create sequence pais_seq;
create sequence estado_seq;

commit;