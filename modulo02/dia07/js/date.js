function rodarPrograma(){
  const h1 = document.getElementById("horario")
  
  const atualizarHorario = function (){
    h1.innerText = moment().locale('pt-br').format('LTS')
  }

  const btnPararRelogio = document.getElementById("btnPararRelogio")

  btnPararRelogio.onclick = function(){
    clearInterval(idIntervalo)
  }

  atualizarHorario()

  const idIntervalo = setInterval( atualizarHorario, 1000)
}

rodarPrograma()
