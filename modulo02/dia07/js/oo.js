function Elfo(nome){
  this.nome = nome;
  //forma errada de atribuir funcao a um objeto
  this.atirarFlecha = function(){
    //atira uma flecha
  }
}

//utilizando PROTOYPE
//forma correta de atribuir funcao a um objeto
Elfo.prototype.atirarFlecha = function(){
  //atira uma flecha
}

//a partir de 2015 javascript implementou
class Elfo{
  constructor(nome){
    this.nome = nome
  }

  atirarFlecha(){
    //atira uma flecha
  }
}
