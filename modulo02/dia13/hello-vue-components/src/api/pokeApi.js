import Pokemon from '../models/pokemon.js'

export default class PokeApi {
  constructor( url ) {
    this.url = url
  }

  // const pokemonsDoTipo = await pokeApi.listarPorTipo( .. )
  async listarPorTipo( idTipo ) {
    const urlTipo = `https://pokeapi.co/api/v2/type/${ idTipo }/`
    return new Promise( resolve => {
      fetch( urlTipo )
        .then( j => j.json() )
        .then( p => {
          // só nos interessa o campo pokemon, que é a lista de pokémons vinda da API.
          resolve( p.pokemon )  
        } )
    } )
  }

  async paginar( listaCompleta, qtdPagina, pagina ) {
    const indiceInicio = qtdPagina * ( pagina - 1 )
    const indiceFim = indiceInicio + qtdPagina
    return new Promise( resolve => {
      const pokemons = listaCompleta.slice( indiceInicio, indiceFim )
      const promisesPkm = pokemons.map( p => this.buscarPorUrl( p.pokemon.url ) )
      Promise.all( promisesPkm ).then( listaPokemons => {
        resolve( listaPokemons )
      } )
    } )
  }

  async buscarPorUrl( urlPokemon ) {
    return new Promise( resolve => {
      fetch( urlPokemon )
        .then( j => j.json() )
        .then( p => {
          const pokemon = new Pokemon( p )
          // return "assícrono",
          // vai jogar o valor de pokemon para quem chamou utilizá-lo de forma assíncrona
          resolve( pokemon )
        } )
    } )
  }

  async buscar( idPokemon ) {
    let urlPokemon = `${ this.url }/${ idPokemon }/`
    return this.buscarPorUrl( urlPokemon )
  }
}
