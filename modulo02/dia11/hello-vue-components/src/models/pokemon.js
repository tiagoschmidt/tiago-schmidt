export default class Pokemon {
  constructor( jsonVindoDaApi ) {
    this.nome = jsonVindoDaApi.name.toUpperCase()
    this.id = jsonVindoDaApi.id
    this.thumbUrl = jsonVindoDaApi.sprites.front_default
    this.altura = jsonVindoDaApi.height * 10
    this.peso = jsonVindoDaApi.weight / 10
    this.tipos = jsonVindoDaApi.types.map( t => t.type.name + " " )
    this.stats = jsonVindoDaApi.stats.map( s => s.stat.name + " " )
  }
}
