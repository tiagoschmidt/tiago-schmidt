import Pokemon from '../models/pokemon.js'

export default class PokeApi {
  constructor( url ) {
    this.url = url
  }

  async listarPorTipo( idTipo, pokemonsPorPagina, paginaAtual ) {
    const urlTipo = `https://pokeapi.co/api/v2/type/${ idTipo }/`
    return new Promise( resolve => {
      fetch( urlTipo )
        .then( j => j.json() )
        .then( t => {
          //paginador
          const limitePaginacao = paginaAtual * pokemonsPorPagina
          const inicioPaginacao = limitePaginacao - pokemonsPorPagina

          //seleciona a pagina
          const pokemons = t.pokemon.slice( inicioPaginacao, limitePaginacao )

          const promisesPkm = pokemons.map( p => this.buscarPorUrl( p.pokemon.url ) )
          Promise.all( promisesPkm )
            .then( resultadoFinal => { 
              resolve( resultadoFinal )
            } )
        } )
      } )
  }

  async buscarPorUrl( urlPokemon ) {
    return new Promise( resolve => {
      fetch( urlPokemon )
        .then( j => j.json() )
        .then( p => {
          const pokemon = new Pokemon( p )
          resolve( pokemon )
        } )
    } )
  }

  async buscar( idPokemon ) {
    let urlPokemon = `${ this.url }/${ idPokemon }/`
    return this.buscarPorUrl( urlPokemon )
  }
}
