import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import Login from './components/screens/Login.vue'
import VeeValidate, {Validator} from 'vee-validate';
import ptBR from 'vee-validate/dist/locale/pt_BR'

import Home from './components/screens/Home.vue'

Vue.config.productionTip = false
Validator.localize( 'pt_BR', ptBR )

Vue.use( VueRouter )
Vue.use(VeeValidate)


const routes = [
  { path: '/', component: Login },
  { name: 'home', path: '/home/:usuario', component: Home }
]

const router = new VueRouter( {
  routes
} )

Validator.extend('email-dbc', {
  getMessage: () => {
    return "Email deve possuir extensão '@dbccompany.com.br'"
  },
  validate: valor => {
    return valor.indexOf('@dbccompany.com.br') !== -1
  }
})

new Vue( {
  router,
  render: h => h(App)
} ).$mount('#app')
