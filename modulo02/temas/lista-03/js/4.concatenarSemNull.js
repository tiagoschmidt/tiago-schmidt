// ==> EXERCICIO 04
function concatenarSemNull( var1, var2 ){
	if(var1 === null)
		var1 = "";
	if(var2 === null)
		var2 = "";
	return var1 + var2;
}