// ==> EXERCICIO 01
function calcularCirculo( circulo ) {
	if(circulo.tipoCalculo === 'A'){
		var area = Math.pow(circulo.raio,2) * Math.PI;
		return Number(area.toFixed(2));
	}
	if(circulo.tipoCalculo === 'C'){
		var circunferencia = circulo.raio * 2 * Math.PI;
		return Number(circunferencia.toFixed(2));
	}
}
