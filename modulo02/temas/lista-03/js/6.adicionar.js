// ==> EXERCICIO 06
function adicionar( x ) {
	if(x === undefined)
		x = 0;
	return ( y ) => {
		if(y === undefined)
			y = 0;
		return x + y;
	};	
}