// ==> EXERCICIO 03
function concatenarSemUndefined( var1, var2 ){
	if(var1 === undefined)
		var1 = "";
	if(var2 === undefined)
		var2 = "";
	return var1 + var2;
}