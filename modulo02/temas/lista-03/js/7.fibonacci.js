// ==> EXERCICIO 07
function fibonacci(n) {
  if(n > 0){
    var j = 0, k = 1;
    for (var i = 2; i <= n; i++) {
      var aux = j;
      j = k;
      k += aux;
    }
    return k;
  }
  else return 0;
}