describe( 'concatenarSemUndefined', function() {

  beforeEach( function() {
    chai.should();
  } )

  it( 'concatenar undefined com Olá mundo!, deve ser Olá mundo!', function() {
    const var1 = undefined;
	const var2 = "Olá, mundo!";
    const resultado = concatenarSemUndefined( var1, var2 );
    resultado.should.equal( "Olá, mundo!" );
  } )

  it( 'concatenar string undefined com Olá!, deve ser undefined Olá!', function() {
    const var1 = 'undefined';
	const var2 = " Olá!";
    const resultado = concatenarSemUndefined( var1, var2 );
    resultado.should.equal( "undefined Olá!" );
  } )
  
  it( 'concatenar Olá, mundo! sem outro parametro, deve ser apenas Olá, mundo!', function() {
	const var1 = "Olá, mundo!";
    const resultado = concatenarSemUndefined( var1 );
    resultado.should.equal( "Olá, mundo!" );
  } )
  
  it( 'concatenar Olá, com mundo!, deve ser Olá, mundo!', function() {
    const var1 = "Olá,";
	const var2 = " mundo!";
    const resultado = concatenarSemUndefined( var1, var2 );
    resultado.should.equal( "Olá, mundo!" );
  } )
  
  it( 'concatenar undefined com undefined, deve ser string vazia', function() {
    const test = [];
	const var1 = test[6];
	const var2 = undefined;
    const resultado = concatenarSemUndefined( var1, var2 );
    resultado.should.equal( "" );
  } )
  
} )