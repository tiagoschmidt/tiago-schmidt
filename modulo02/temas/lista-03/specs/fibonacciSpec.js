describe( 'fibonacci', function() {

  beforeEach( function() {
    chai.should();
  } )

  it( 'fibonacci de 1, deve ser 1', function() {
    const num = 1;
    const resultado = fibonacci( num );
    resultado.should.equal( 1 );
  } )
  
  it( 'fibonacci de 2, deve ser 1', function() {
	  const num = 2;
    const resultado = fibonacci( num );
    resultado.should.equal( 1 );
  } )

  it( 'fibonacci de 0, deve ser 0', function() {
    const num = 0;
    const resultado = fibonacci( num );
    resultado.should.equal( 0 );
  } )
  
  it( 'fibonacci de 8, deve ser 21', function() {
	  const num = 8;
    const resultado = fibonacci( num );
    resultado.should.equal( 21 );
  } )
  
  it( 'fibonacci de 18, deve ser 2584', function() {
    const num = 18;
    const resultado = fibonacci( num );
    resultado.should.equal( 2584 );
  } )

  it( 'fibonacci de string, deve ser 0', function() {
    const num = "string qualquer";
    const resultado = fibonacci( num );
    resultado.should.equal( 0 );
  } )

  it( 'fibonacci de 6.125, deve ser 8', function() {
    const num = 6.125;
    const resultado = fibonacci( num );
    resultado.should.equal( 8 );
  } )
  
} )
