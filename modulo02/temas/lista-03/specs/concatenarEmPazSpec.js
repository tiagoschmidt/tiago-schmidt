describe( 'concatenarEmPaz', function() {

  beforeEach( function() {
    chai.should();
  } )

  it( 'concatenar null com Olá mundo!, deve ser Olá mundo!', function() {
    const var1 = null;
	const var2 = "Olá, mundo!";
    const resultado = concatenarEmPaz( var1, var2 );
    resultado.should.equal( "Olá, mundo!" );
  } )
  
  it( 'concatenar undefined com Olá mundo!, deve ser Olá mundo!', function() {
    const var1 = undefined;
	const var2 = "Olá, mundo!";
    const resultado = concatenarEmPaz( var1, var2 );
    resultado.should.equal( "Olá, mundo!" );
  } )

  it( 'concatenar string null com string undefined, deve ser undefined null', function() {
    const var1 = "undefined ";
	const var2 = "null";
    const resultado = concatenarEmPaz( var1, var2 );
    resultado.should.equal( "undefined null" );
  } )
  
  it( 'concatenar Olá, sem outro parametro, deve ser Olá, ', function() {
	const var1 = "Olá, ";
    const resultado = concatenarEmPaz( var1 );
    resultado.should.equal( "Olá, " );
  } )
  
  it( 'concatenar Olá, com mundo!, deve ser Olá, mundo!', function() {
    const var1 = "Olá,";
	const var2 = " mundo!";
    const resultado = concatenarEmPaz( var1, var2 );
    resultado.should.equal( "Olá, mundo!" );
  } )
  
  it( 'concatenar undefined com null, deve ser string vazia', function() {
    const test = [];
	const var1 = test[6];
	const var2 = null;
    const resultado = concatenarEmPaz( var1, var2 );
    resultado.should.equal( "" );
  } )
  
} )