describe( 'adicionar', function() {

  beforeEach( function() {
    chai.should();
  } )

  it( 'somar (1) com (3), deve ser 4', function() {
    const var1 = 1;
	  const var2 = 3;
    const resultado = adicionar( var1 )( var2 );
    resultado.should.equal( 4 );
  } )
  
  it( 'somar ( ) com (3), deve ser 3', function() {
	  const var1 = 3;
    const resultado = adicionar(  ) ( var1 );
    resultado.should.equal( 3 );
  } )

  it( 'somar ( ) com ( ), deve ser 0', function() {
    const resultado = adicionar(  ) (  );
    resultado.should.equal( 0 );
  } )
  
  it( 'somar ( 5 ) com ( ), deve ser 5', function() {
	  const var1 = 5;
    const resultado = adicionar( var1 )(  );
    resultado.should.equal( 5 );
  } )
  
  it( 'somar Olá, com mundo!, deve ser Olá, mundo!', function() {
    const var1 = "Olá,";
	  const var2 = " mundo!";
    const resultado = adicionar( var1 ) ( var2 );
    resultado.should.equal( "Olá, mundo!" );
  } )
  
} )
