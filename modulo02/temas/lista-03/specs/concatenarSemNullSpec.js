describe( 'concatenarSemNull', function() {

  beforeEach( function() {
    chai.should();
  } )

  it( 'concatenar null com Olá mundo!, deve ser Olá mundo!', function() {
    const var1 = null;
	const var2 = "Olá, mundo!";
    const resultado = concatenarSemNull( var1, var2 );
    resultado.should.equal( "Olá, mundo!" );
  } )

  it( 'concatenar string null com Olá!, deve ser null Olá!', function() {
    const var1 = 'null';
	const var2 = " Olá!";
    const resultado = concatenarSemNull( var1, var2 );
    resultado.should.equal( "null Olá!" );
  } )
  
  it( 'concatenar Olá, sem outro parametro, deve ser Olá, undefined', function() {
	const var1 = "Olá, ";
    const resultado = concatenarSemNull( var1 );
    resultado.should.equal( "Olá, undefined" );
  } )
  
  it( 'concatenar Olá, com mundo!, deve ser Olá, mundo!', function() {
    const var1 = "Olá,";
	const var2 = " mundo!";
    const resultado = concatenarSemNull( var1, var2 );
    resultado.should.equal( "Olá, mundo!" );
  } )
  
  it( 'concatenar null com null, deve ser string vazia', function() {
    const test = [];
	test[6] = null;
	const var1 = test[6];
	const var2 = null;
    const resultado = concatenarSemNull( var1, var2 );
    resultado.should.equal( "" );
  } )
  
} )