describe( 'naoBissexto', function() {

  // antes de cada teste, chama a biblioteca "chai" para poder usar as asserções de forma mais amigável (should, expect)
  // documentação: https://www.chaijs.com/
  beforeEach( function() {
    chai.should()
  } )

  it( 'ano 2016 eh bissexto, deve ser false', function() {
    const ano = 2016;
    const resultado = naoBissexto( ano )
    resultado.should.equal( false )
  } )

  it( 'ano 2017 nao eh bissexto, deve ser true', function() {
	const ano = 2017;
    const resultado = naoBissexto( ano )
    resultado.should.equal( true )
  } )
  
  it( 'ano 1900 nao eh bissexto, deve ser true', function() {
	const ano = 1900;
	const resultado = naoBissexto( ano )
	resultado.should.equal( true )
  } )
  
  it( 'ano 1600 eh bissexto, deve ser false', function(){
	const ano = 1600;
	const resultado = naoBissexto( ano )
	resultado.should.equal( false )
  } )
  
} )
