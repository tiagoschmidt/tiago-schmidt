const urlApi = 'https://pokeapi.co/api/v2/pokemon'
const pokeApi = new PokeApi( urlApi )

let app = new Vue( {
  el: '#meuPrimeiroApp',
  data: {
    idParaBuscar: "",
    pokemon: {}
  },
  methods:{
    async buscar(){
      let idPokemonAtual = document.querySelector("#pokemonAtual").innerText
      idPokemonAtual += " "
      if(idPokemonAtual.indexOf(` ${this.idParaBuscar} `) === -1){
        if(this.idParaBuscar>0 && this.idParaBuscar<803)
          this.pokemon = await pokeApi.buscar( this.idParaBuscar )
        else
          alert("Digite um id válido!")
      }
    },
    async gerarPokemonAleatorio(){
      this.pokemon = await pokeApi.gerarPokemonAleatorio()
    } 
  }
} )
