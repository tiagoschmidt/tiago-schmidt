class PokeApi {
  constructor( url ) {
    this.url = url
    if(!localStorage.idPokemon){
      localStorage.idPokemon = 0
      localStorage.qtdPokemons = 0
    }
  }
  async buscar( idPokemon ) {
    let urlPokemon = `${ this.url }/${ idPokemon }/`
    return new Promise( resolve => {
      fetch(urlPokemon)
        .then( j => j.json())
        .then(p => {
          const pokemon = new Pokemon(p)
          resolve( pokemon )
        } )
    } )
  }
  async gerarPokemonAleatorio(){
    let temQueBuscar = true    
    while(temQueBuscar){
      if(localStorage.qtdPokemons<802){
        var idAleatorio = Math.random() * (802 - 1) +1
        idAleatorio = parseInt(idAleatorio.toFixed())
        const stringId = ` ${idAleatorio} `
        if(localStorage.idPokemon.indexOf(stringId) === -1){
          localStorage.idPokemon += ` ${idAleatorio} `
          localStorage.qtdPokemons++
          return this.buscar( idAleatorio )
        }
      }
      else{
        alert("Todas possibilidades já foram geradas. Esvazie seu LocalStorage para continuar gerando pokemons aleatoriamente.")
        temQueBuscar = false
      }
    }
  }
}
