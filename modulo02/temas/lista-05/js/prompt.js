const meuH2 = document.getElementById("tituloPagina")

function solicitarNome(){
  localStorage.nome = prompt("Qual o seu nome?")
  imprimirNome()
}

function imprimirNome(){
  meuH2.innerText = localStorage.nome
}

function removerNome(){  
  if(localStorage.nome){
    localStorage.removeItem("nome")
    meuH2.innerText = "Sem Nome"
    localStorage.qtdClicks ++
    if(localStorage.qtdClicks >= 5)
      alterarEstadoBotoes()
  }
}

function reabilitaBotao(){
  buttonClear.disabled=false
  buttonEnable.disabled=true
  localStorage.qtdClicks=0
}

const buttonClear = document.getElementById("limpar-dados")
const buttonEnable = document.getElementById("habilitar-botao")
const deveInicializarContador = !(localStorage.qtdClicks)
const naoDeveSolicitar = localStorage.nome && localStorage.nome !== ""
const deveAlterar = localStorage.qtdClicks >= 5

buttonClear.onclick = removerNome
buttonEnable.onclick = reabilitaBotao

function alterarEstadoBotoes(){
    buttonClear.disabled=true
    buttonEnable.disabled=false 
}

if(deveInicializarContador)
  localStorage.qtdClicks = 0

if(deveAlterar)
  alterarEstadoBotoes()

if(naoDeveSolicitar)
  imprimirNome()
else
  solicitarNome()
