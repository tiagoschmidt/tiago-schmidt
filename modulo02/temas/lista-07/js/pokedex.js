function rodarPrograma() {
  
  const $dadosPokemon = document.getElementById( 'dadosPokemon' )
  const $h1 = $dadosPokemon.querySelector( 'h1' )
  const $h2Type = document.createElement( 'h2' )
  const $h2Stat = document.createElement( 'h2' )
  const $img = $dadosPokemon.querySelector( '#thumb' )
  const $inputId = document.getElementById( 'inputId' )
  const $ulTipos = document.createElement( 'ul' )
  const $ulEstatisticas = document.createElement( 'ul' )
  const $mostraDados = document.querySelector('#mostra-dados')

  //li's
  const liId = document.createElement('li')
  const liAltura = document.createElement('li')
  const liPeso = document.createElement('li')
  //ul's
  

  const url = 'https://pokeapi.co/api/v2/pokemon'
  const pokeApi = new PokeApi( url )
 
  $inputId.onblur = () => {

    const idDigitado = $inputId.value

	if(idDigitado > 0 && idDigitado < 803)
		pokeApi.buscar( idDigitado )
		  .then( res => res.json() )
		  .then( dadosJson => {
			const pokemon = new Pokemon( dadosJson )
			renderizarPokemonNaTela( pokemon )
		  } )
	else
		alert("Digite um id válido.")
  }

  function renderizarPokemonNaTela( pokemon ) {

    $ulTipos.innerHTML = ''
    $ulEstatisticas.innerHTML = ''

    //nome e foto
    $h1.innerText = pokemon.nome
    $img.src = pokemon.thumbUrl
    //li's dados principais
    liId.innerText = `ID: ${pokemon.id}`
    $mostraDados.appendChild(liId)    
    liAltura.innerText = `Altura: ${pokemon.altura}cm`
    $mostraDados.appendChild(liAltura)    
    liPeso.innerText = `Peso: ${pokemon.peso}kg(s)`
    $mostraDados.appendChild(liPeso)
    //li's tipos
    $h2Type.innerText = "Tipos:"
    $mostraDados.appendChild($h2Type)
    $mostraDados.appendChild($ulTipos)
    
    //li's stats
    $h2Stat.innerText = "Estatísticas:"
    $mostraDados.appendChild($h2Stat)
    $mostraDados.appendChild($ulEstatisticas)
    
    
    //li's tipos
    pokemon.tipos.forEach(tipo => {
      let li = document.createElement('li')
      li.innerText = tipo
      $ulTipos.appendChild(li)
    });
      
    //li's estatisticas
    pokemon.stats.forEach(stat => {
      let li = document.createElement('li')
      li.innerText = stat
      $ulEstatisticas.appendChild(li)
    });
	
	


  }

}

rodarPrograma()
