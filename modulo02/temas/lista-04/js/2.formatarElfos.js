function formatarElfos( arrayElfos ){
  arrayElfos.forEach( elfo => {
    elfo.nome = elfo.nome.toUpperCase()
    elfo.temExperiencia = elfo.experiencia > 0
    delete elfo.experiencia

    const textoExperiencia = elfo.temExperiencia ? "com" : "sem"
    const textoFlecha = elfo.qtdFlechas !== 1 ? "s" : ""

    elfo.descricao = `${elfo.nome} ${textoExperiencia} experiencia e com ${elfo.qtdFlechas} flecha${textoFlecha}.`    
  } )
  return arrayElfos
}
