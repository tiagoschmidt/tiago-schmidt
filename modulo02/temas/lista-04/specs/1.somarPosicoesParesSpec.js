describe( 'somarPosicoesPares', function(){

  beforeEach( function() {
    chai.should()
  } )

  it( 'somar pares do array [ 1, 56, 4.34, 6, -2 ], deve ser 3.34', function(){
    const arrayTest = [ 1, 56, 4.34, 6, -2 ]
    const resultado = somarPosicoesPares( arrayTest )
    resultado.should.equal( 3.34 )
  } )

  it( 'somar pares do array [ 56, 4.34, 6, -2 ], deve ser 62', function(){
    const arrayTest = [ 56, 4.34, 6, -2 ]
    const resultado = somarPosicoesPares( arrayTest )
    resultado.should.equal( 62 )
  } )

  it( 'somar pares do array [ 56, 100, -56, 200, 56, 300, -56 ], deve ser 0', function(){
    const arrayTest = [ 56, 100, -56, 200, 56, 300, -56 ]
    const resultado = somarPosicoesPares( arrayTest )
    resultado.should.equal( 0 )
  } )

  it( 'somar pares de um array vazio, deve ser 0', function(){
    const arrayTest = [ ]
    const resultado = somarPosicoesPares( arrayTest )
    resultado.should.equal( 0 )
  } )

} )
