describe( 'formatarElfos', function(){

  beforeEach( function() {
    chai.should()
  } )

  it( 'formatar Legolas sem exp e com 6 flechas e Galadriel com exp e 1 flecha', function(){
    const elfo1 = { nome: "Legolas"  , experiencia: 0, qtdFlechas: 6 }
    const elfo2 = { nome: "Galadriel", experiencia: 1, qtdFlechas: 1 }
    const arrayElfos = [ elfo1, elfo2 ]
    const arrayFormatado = formatarElfos( arrayElfos )

    const elfoTest1 = { nome: "LEGOLAS", temExperiencia: false, qtdFlechas: 6, descricao: "LEGOLAS sem experiencia e com 6 flechas." }
    const elfoTest2 = { nome: "GALADRIEL", temExperiencia: true, qtdFlechas: 1, descricao: "GALADRIEL com experiencia e com 1 flecha." }

    const arrayEsperado = [ elfoTest1, elfoTest2 ]
  
    chai.expect(arrayEsperado).to.eql(arrayFormatado)
  } )

  it( 'formatar Legolas com exp e com 1 flecha e Galadriel sem exp e 0 flechas', function(){
    const elfo1 = { nome: "Legolas"  , experiencia: 2, qtdFlechas: 1 }
    const elfo2 = { nome: "Galadriel", experiencia: 0, qtdFlechas: 0 }
    const arrayElfos = [ elfo1, elfo2 ]
    const arrayFormatado = formatarElfos( arrayElfos )

    const elfoTest1 = { nome: "LEGOLAS", temExperiencia: true, qtdFlechas: 1, descricao: "LEGOLAS com experiencia e com 1 flecha." }
    const elfoTest2 = { nome: "GALADRIEL", temExperiencia: false, qtdFlechas: 0, descricao: "GALADRIEL sem experiencia e com 0 flechas." }

    const arrayEsperado = [ elfoTest1, elfoTest2 ]
  
    chai.expect(arrayEsperado).to.eql(arrayFormatado)
  } )

  it( 'formatar Legolas sem exp e com 0 flechas e Galadriel sem exp e 0 flechas', function(){
    const elfo1 = { nome: "Legolas"  , experiencia: 0, qtdFlechas: 0 }
    const elfo2 = { nome: "Galadriel", experiencia: 0, qtdFlechas: 0 }
    const arrayElfos = [ elfo1, elfo2 ]
    const arrayFormatado = formatarElfos( arrayElfos )

    const elfoTest1 = { nome: "LEGOLAS", temExperiencia: false, qtdFlechas: 0, descricao: "LEGOLAS sem experiencia e com 0 flechas." }
    const elfoTest2 = { nome: "GALADRIEL", temExperiencia: false, qtdFlechas: 0, descricao: "GALADRIEL sem experiencia e com 0 flechas." }

    const arrayEsperado = [ elfoTest1, elfoTest2 ]
  
    chai.expect(arrayEsperado).to.eql(arrayFormatado)
  } )

  it( 'formatar Legolas com exp e com 600 flechas e Galadriel com exp e 100 flechas', function(){
    const elfo1 = { nome: "Legolas"  , experiencia: 10, qtdFlechas: 600 }
    const elfo2 = { nome: "Galadriel", experiencia: 10, qtdFlechas: 100 }
    const arrayElfos = [ elfo1, elfo2 ]
    const arrayFormatado = formatarElfos( arrayElfos )

    const elfoTest1 = { nome: "LEGOLAS", temExperiencia: true, qtdFlechas: 600, descricao: "LEGOLAS com experiencia e com 600 flechas." }
    const elfoTest2 = { nome: "GALADRIEL", temExperiencia: true, qtdFlechas: 100, descricao: "GALADRIEL com experiencia e com 100 flechas." }

    const arrayEsperado = [ elfoTest1, elfoTest2 ]
  
    chai.expect(arrayEsperado).to.eql(arrayFormatado)
  } )

} )
