function rodarPrograma(){
  class Relogio{
    constructor(){
      this.pausar = function(){
        clearInterval(idIntervalo)
        btnPararRelogio.disabled=true
        btnReiniciarRelogio.disabled=false
        h1.className = "tempo-acabando"
      }

      this.reiniciar = function(){
        atualizarHorario()
        idIntervalo = setInterval( atualizarHorario, 1000)
        btnPararRelogio.disabled=false
        btnReiniciarRelogio.disabled=true
        h1.className -= "tempo-acabando"
      }
    }
  }

  const relogio = new Relogio()
  const h1 = document.getElementById("horario")
  
  const atualizarHorario = function (){
    h1.innerText = moment().locale('pt-br').format('LTS')
  }

  const btnPararRelogio = document.getElementById("btnPararRelogio")
  const btnReiniciarRelogio = document.getElementById("btnReiniciarRelogio")

  btnReiniciarRelogio.disabled=true

  btnPararRelogio.onclick = relogio.pausar
  btnReiniciarRelogio.onclick = relogio.reiniciar  

  atualizarHorario()

  var idIntervalo = setInterval( atualizarHorario, 1000)
}

rodarPrograma()
