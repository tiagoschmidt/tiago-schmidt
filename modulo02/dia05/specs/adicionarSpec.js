describe( 'adicionar', function() {
  const expect = chai.expect
  beforeEach( function() {
    chai.should()
  } )

  it( 'adicionar 3 e 4', function() {
    adicionar( 3 )( 4 ).should.equal( 7 )
  } )

  it( 'adicionar 5642 e 8749', function() {
    adicionar( 5642 )( 8749 ).should.equal( 14391 )
  } )

  it( 'adicionar -1 e 2', function() {
    adicionar( -1 )( 2 ).should.equal( 1 )
  } )

} )
