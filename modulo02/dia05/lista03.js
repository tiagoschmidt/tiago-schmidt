function calcularCirculo( parametros ) {

  const funcoes = {
    'A': raio => Math.PI * parametros.raio ** 2,
    'C': raio => 2 * Math.PI * parametros.raio
  }
  const funcao = funcoes[ parametros.tipoCalculo ]
  return typeof funcao === 'function' ? funcao( parametros.raio ) : undefined
}


function naoBissexto() {
  // toda função tem a variável arguments disponível, com os argumentos utilizados para chamá-la.
  const ano = arguments[ 0 ]
  const bissexto = ( ( ano % 4 == 0 && ano % 100 != 0 ) || (ano % 400 == 0) )
  return !bissexto
}

function concatenarSemUndefined( texto1 = '', texto2 = '' ) {
  //return texto1 + texto2
  return `${ texto1 }${ texto2 }`
}

function concatenarSemNull( texto1, texto2 ) {
  texto1 = texto1 !== null ? texto1 : ''
  texto2 = texto2 !== null ? texto2 : ''
  return texto1 + texto2
}

function concatenarEmPaz( texto1, texto2 ) {
  // truthy or falsy
  // caso texto1 esteja com valor, não seja undefined ou null, pega seu valor.. caso contrário ''
  texto1 = texto1 || ''
  texto2 = texto2 || ''
  return texto1 + texto2
}

// currying
// var funcao = adicionar(3)
// funcao(4) // retorna 7
// adicionar(3)(4)
function adicionar( numero1 ) {
  // closure, fechamento
  // linguagem de programação: clojure
  return function( numero2 ) {
    return numero1 + numero2
  }
}

// soma do enésimo elemento é a soma do valor n + somatório até n-1
// IIFE
function fiboSum( n ) {
  console.log( `executou` )
  function fibonacci( n ) {
    if ( n === 1 || n === 2 ) {
      return 1
    }
  
    return fibonacci( n - 1 ) + fibonacci( n - 2 )
  }

  if ( n === 1 ) {
    return 1
  }
  return fibonacci( n ) + fiboSum( n - 1 )
}

// 4
// 1 + 1 + 2 + 3

// 7
// 1 + 1 + 2 + 3 + 5 + 8 + 13 
