class Jedi{
  constructor(nome){
    this.nome = nome
    this.h1 = document.createElement("h1")
    const dadosJedi = document.getElementById("dadosJedi")
    this.h1.innerText = this.nome
    this.h1.id = `jedi_${this.nome}`
    dadosJedi.appendChild( this.h1 )
  }

  atacarComSelf(){
    let self = this
    setTimeout(function(){ 
      self.h1.innerText += " atacou!"
    }, 1000 )    
  }

  atacarComBind(){
    setTimeout(function(){ 
      this.h1.innerText += " atacou!"
    }.bind(this), 1000 )    
  }

  atacarComArrowFunction(){
    setTimeout( () => { 
      this.h1.innerText += " atacou!"
    }, 1000 )    
  }
}
