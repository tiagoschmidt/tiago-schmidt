function rodarPrograma(){

  const $dadosPokemon = document.getElementById("dadosPokemon")
  const $h1 = $dadosPokemon.querySelector("h1")
  const $img = $dadosPokemon.querySelector("#thumb")
  const $input = document.getElementById("idPokemon")

  $input.focus()

  $input.onblur = () => {

    let url = `https://pokeapi.co/api/v2/pokemon/${$input.value}/`
    
    if($input.checkValidity() && ($input.value > 0 && $input.value <803)){
      fetch(url)
        .then( res => res.json())
        .then( dadosJson => {
          $h1.innerText = dadosJson.name.toUpperCase()
          $img.src = dadosJson.sprites.front_default
        })
    } else{
      $input.focus()
    }
  }
}

rodarPrograma()
